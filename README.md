Scripts and simulation configuration files for power analysis of artificial 
selection experiments.

The simulations use the forqs forward simulator, and the scripts are used in
downstream analysis of the simulations.


