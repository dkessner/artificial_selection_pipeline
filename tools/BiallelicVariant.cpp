//
// BiallelicVariant.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2014 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BiallelicVariant.hpp"
#include "boost/tokenizer.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <fstream>
#include <cstring>
#include <numeric>


using namespace std;
using namespace boost::lambda; // for _1


namespace {


const size_t haplotype_count_ = 162; // hard-coded
const size_t bits_per_int_ = sizeof(int)*8;
                

vector<string> tokenize_commas(const string& s)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
    tokenizer tokens(s, sep);
    vector<string> result;
    copy(tokens.begin(), tokens.end(), back_inserter(result));
    return result;
}


} // namespace


//
// BiallelicVariant
//


BiallelicVariant::BiallelicVariant(const string& line)
:   position(0), reference('\0')
{
    memset(bits, 0, sizeof(bits));
    memset(dummy, 0, sizeof(dummy));
    
    if (!line.empty())
    {
        vector<string> tokens = tokenize_commas(line);
        if (tokens.size() != haplotype_count_ + 4)
        {
            copy(tokens.begin(), tokens.end(), ostream_iterator<string>(cout, " "));
            throw runtime_error("[BiallelicVariant] Unexpected number of tokens.");
        }

        position = atoi(tokens[0].c_str());
        reference = tokens[1][0];

        vector<char> bases(haplotype_count_);
        transform(tokens.begin()+2, tokens.end()-2, bases.begin(), _1[0]); // string -> char

        vector<char>::const_iterator base = bases.begin();
        for (size_t h=0; h<haplotype_count_; ++h, ++base)
        switch(*base)
        {
            case('A'):
            case('C'):
            case('G'):
            case('T'):
                if (*base != reference) set_bit(h, true);
                break;
            case('R'):
            case('Y'):
            case('K'):
            case('M'):
            case('S'):
            case('W'):
            case('B'):
            case('D'):
            case('H'):
            case('V'):
            case('N'):
                break; // ambiguous == reference
            default:
                throw runtime_error("[BiallelicVariant] Unknown base.");
        }
    }
}


bool BiallelicVariant::get_bit(size_t index) const
{
    if (index >= haplotype_count_)
        throw runtime_error("[BiallelicVariant] Bad index.");
    size_t int_index = index / bits_per_int_; 
    size_t position = index % bits_per_int_;
    return bits[int_index] & (1 << position);
}


void BiallelicVariant::set_bit(size_t index, bool value)
{
    if (index >= haplotype_count_)
        throw runtime_error("[BiallelicVariant] Bad index.");
    size_t int_index = index / bits_per_int_; 
    size_t position = index % bits_per_int_;
    bits[int_index] |= (int(value) << position);
}


double BiallelicVariant::allele_frequency(vector<unsigned int> counts) const
{
    if (counts.size() != haplotype_count_)
        throw runtime_error("[BiallelicVariant] Bad counts size.");

    unsigned int nonref_count = 0;

    vector<unsigned int>::const_iterator count = counts.begin();
    for (size_t h=0; h<haplotype_count_; ++h, ++count)
        if (get_bit(h))
            nonref_count += *count;

    unsigned int total = accumulate(counts.begin(), counts.end(), 0);

    return double(nonref_count) / total;
}


string BiallelicVariant::bit_string() const
{
    string result(haplotype_count_, '0');
    for (size_t i=0; i<haplotype_count_; ++i)
        if (get_bit(i)) 
            result[i] = '1';
    return result;
}


bool operator==(const BiallelicVariant& a, const BiallelicVariant& b)
{
    for (size_t i=0; i<sizeof(a.bits)/sizeof(int); ++i)
        if (a.bits[i] != b.bits[i]) return false;
    return a.position == b.position && a.reference == b.reference;
}


ostream& operator<<(ostream& os, const BiallelicVariant& variant)
{
    os << variant.position << " " << variant.reference << " " << variant.bit_string();
    return os;
}


//
// BiallelicVariants
//


namespace {

bool is_header_line(const string& buffer)
{
    string first_two = buffer.substr(0,2);

    return (first_two == "2L" ||
            first_two == "2R" ||
            first_two == "3L" ||
            first_two == "3R" ||
            first_two == "X,");
}

} // namespace


BiallelicVariants::BiallelicVariants(const string& filename)
{
    if (!filename.empty()) read_binary(filename);
}


void BiallelicVariants::read_text(const string& filename)
{
    ifstream is(filename.c_str());
    if (!is)
        throw runtime_error("[BiallelicVariants] Unable to open file " + filename);

    while(is)
    {
        string buffer;
        getline(is, buffer);
        if (!is) break;
        if (buffer.empty() || is_header_line(buffer)) continue;
        push_back(BiallelicVariant(buffer));
    }
}


namespace {

const char* magic_ = "BiallelicVariants\n\0";

struct BinaryHeader
{
    char magic[20];
    unsigned int record_count;

    BinaryHeader()
    :   record_count(0)
    {
        memset(magic, 0, sizeof(magic));
    }
};

} // namespace


void BiallelicVariants::read_binary(const string& filename)
{
    ifstream is(filename.c_str());
    if (!is)
        throw runtime_error("[BiallelicVariants] Unable to open file " + filename);

    BinaryHeader header;
    is.read((char*)&header, sizeof(header));

    if (!is || strcmp(header.magic, magic_))
        throw runtime_error("[BiallelicVariants] Bad header in file " + filename);

    cerr << "[BiallelicVariants] Reading " << header.record_count << " records.\n";

    resize(header.record_count);

    is.read((char*)&front(), sizeof(BiallelicVariant) * size());
}


void BiallelicVariants::write_binary(const std::string& filename)
{
    BinaryHeader header;
    memcpy(header.magic, magic_, sizeof(header.magic));
    header.record_count = size();
    
    ofstream os(filename.c_str());
    if (!os)
        throw runtime_error("[BiallelicVariants] Unable to open file " + filename);

    os.write((const char*)&header, sizeof(header));
    if (!empty())
        os.write((const char*)&front(), size() * sizeof(BiallelicVariant));
    os.close();
}


