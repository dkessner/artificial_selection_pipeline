//
// BiallelicVariant.hpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2014 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#ifndef _BIALLELICVARIANT_HPP_
#define _BIALLELICVARIANT_HPP_


#include <string>
#include <vector>


struct BiallelicVariant
{
    int position;
    char reference; // A, C, G, T
    char dummy[3];
    int bits[6];

    BiallelicVariant(const std::string& line = "");

    bool get_bit(size_t index) const;
    void set_bit(size_t index, bool value);

    double allele_frequency(std::vector<unsigned int> counts) const;
    std::string bit_string() const;
};


bool operator==(const BiallelicVariant& a, const BiallelicVariant& b);
std::ostream& operator<<(std::ostream& os, const BiallelicVariant& variant);


class BiallelicVariants : public std::vector<BiallelicVariant>
{
    public:

    BiallelicVariants(const std::string& filename = "");

    void read_text(const std::string& filename);

    void read_binary(const std::string& filename);
    void write_binary(const std::string& filename);
};


#endif // _BIALLELICVARIANT_HPP_

