//
// calculate_dgrp_nonref_allele_frequencies_from_binary.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2014 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BiallelicVariant.hpp"
#include <iostream>
#include <limits>


using namespace std;


int main(int argc, char* argv[])
{
    try
    {
        if (argc<2)
        {
            cout << "Usage: calculate_dgrp_nonref_allele_frequencies_from_binary <filename.bin>\n";
            return 0;
        }

        const char* filename = argv[1];

        BiallelicVariants variants(filename);
        vector<unsigned int> counts(162, 1);
        
        cout << "position ref nonref_allele_freq\n";
        cout.precision(12);
        for (BiallelicVariants::const_iterator it=variants.begin(); it!=variants.end(); ++it)
        {
            double allele_frequency = it->allele_frequency(counts);
            if (allele_frequency == 0)
                cout << it->position << " " << it->reference << " " << "0.0" << endl;
            else if (allele_frequency == 1)
                cout << it->position << " " << it->reference << " " << "1.0" << endl;
            else
                cout << it->position << " " << it->reference << " " << allele_frequency << endl;
        }

        return 0;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch(...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


