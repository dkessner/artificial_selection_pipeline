//
// dgrp_derived_population_allele_frequencies.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2014 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BiallelicVariant.hpp"
#include "Population_ChromosomePairs.hpp" // forqs
#include <boost/filesystem.hpp>
#include <iostream>
#include <iterator>


using namespace std;
namespace bfs = boost::filesystem;


vector<unsigned int> calculate_histogram(const vector<unsigned int> id_list,
                                         unsigned int id_end)
{
    vector<unsigned int> result(id_end);

    for (vector<unsigned int>::const_iterator id=id_list.begin(); id!=id_list.end(); ++id)
    {
        if (*id >= id_end) throw runtime_error("[histogram] Invalid id.");
        ++result[*id];
    }

    return result;
}


vector<unsigned int> get_id_list(const Population& population, 
                                 size_t chromosome_pair_index,
                                 unsigned int position)
{
    vector<unsigned int> result;
    result.reserve(population.population_size() * 2);

    for (const ChromosomePairRangeIterator it=population.begin(); it!=population.end(); ++it)
    {
        const ChromosomePair* chromosome_pair = it->begin() + chromosome_pair_index;

        unsigned int id = chromosome_pair->first.find_haplotype_chunk(position)->id;
        result.push_back(id);

        id = chromosome_pair->second.find_haplotype_chunk(position)->id;
        result.push_back(id);
    }
 
    return result;
}


vector<unsigned int> translate_id_list(const vector<unsigned int>& from,
                                       const vector<unsigned int>& to)
{
    vector<unsigned int> result(from.size());
    unsigned int id_end = to.size();

    vector<unsigned int>::iterator newid=result.begin();
    for (vector<unsigned int>::const_iterator id=from.begin(); id!=from.end(); ++id, ++newid)
    {
        if (*id >= id_end) throw runtime_error("[translate_id_list] Invalid id.");
        *newid = to[*id];
    }
    
    return result;
}


struct ChromosomeArmInfo
{
    const char* arm;
    size_t chromosome_pair_index;
    unsigned int position_offset;
};


enum {ChrX, Chr2L, Chr2R, Chr3L, Chr3R};


ChromosomeArmInfo arm_infos[] =
{
    {"X", 0, 0},
    {"2L", 1, 0},
    {"2R", 1, 25000000},
    {"3L", 2, 0},
    {"3R", 2, 25000000}
};


void calculate_allele_frequencies(const ChromosomeArmInfo& arm_info, 
                                  const BiallelicVariants& variants,
                                  const Population& population,
                                  const Population& founders)
{
    const unsigned int haplotype_count = 162;

    for (BiallelicVariants::const_iterator it=variants.begin(); it!=variants.end(); ++it)
    {
        unsigned int position = it->position + arm_info.position_offset;

        vector<unsigned int> founder_id_list = get_id_list(founders, 
                                                           arm_info.chromosome_pair_index, 
                                                           position);

        vector<unsigned int> id_list = get_id_list(population, 
                                                   arm_info.chromosome_pair_index, 
                                                   position);

        vector<unsigned int> ids_translated = translate_id_list(id_list, founder_id_list);
        vector<unsigned int> allele_counts = calculate_histogram(ids_translated, haplotype_count);
        double allele_frequency = it->allele_frequency(allele_counts);

        // DGRP arm, position
        // cout << arm_info.arm << " " << it->position << " " << allele_frequency << endl;

        // forqs chromosome_pair_index, position
        cout << arm_info.chromosome_pair_index << " " << arm_info.position_offset + it->position << " " << allele_frequency << endl;
    }
}


void test1()
{
    cout << "test1()\n";

    Population_ChromosomePairs founders("test_files_allele_frequencies/1.founders.pop");
    vector<unsigned int> founder_id_list = get_id_list(founders, 1, 10000016);

    cout << "founders:\n";
    copy(founder_id_list.begin(), founder_id_list.end(), ostream_iterator<unsigned>(cout, " "));
    cout << endl;

    Population_ChromosomePairs population("test_files_allele_frequencies/population_gen0_pop1.txt");

    vector<unsigned int> id_list = get_id_list(population, 1, 10000016);

    cout << "ids:\n";
    copy(id_list.begin(), id_list.end(), ostream_iterator<unsigned>(cout, " "));
    cout << endl;

    vector<unsigned int> ids_translated = translate_id_list(id_list, founder_id_list);
    cout << "ids:\n";
    copy(ids_translated.begin(), ids_translated.end(), ostream_iterator<unsigned>(cout, " "));
    cout << endl;

    vector<unsigned int> histogram = calculate_histogram(ids_translated, 162);

    cout << "histogram:\n";
    copy(histogram.begin(), histogram.end(), ostream_iterator<unsigned>(cout, " "));
    cout << endl;

    BiallelicVariants variants_2L("test_files_allele_frequencies/2L.biallelic_variants.bin");

    double allele_frequency = variants_2L[0].allele_frequency(histogram);
    cout << "allele frequency: " << allele_frequency << endl;

    if (allele_frequency != .3)
        throw runtime_error("test1() failed");
    else
        cout << "test1() OK\n";
}


void test2()
{
    cout << "test2()\n";

    BiallelicVariants variants_2L("test_files_allele_frequencies/2L.biallelic_variants.bin");

    size_t chromosome_pair_index = 1;
    unsigned int position = 10000016;
    const size_t haplotype_count = 162;

    Population_ChromosomePairs founders("test_files_allele_frequencies/1.founders.pop");
    vector<unsigned int> founder_id_list = get_id_list(founders, chromosome_pair_index, position);

    Population_ChromosomePairs population("test_files_allele_frequencies/population_gen2_pop1.txt");
    vector<unsigned int> id_list = get_id_list(population, chromosome_pair_index, position);

    vector<unsigned int> ids_translated = translate_id_list(id_list, founder_id_list);
    vector<unsigned int> allele_counts = calculate_histogram(ids_translated, haplotype_count);

    double allele_frequency = variants_2L[0].allele_frequency(allele_counts);
    cout << "allele frequency: " << allele_frequency << endl;

    if (allele_frequency != .58)
        throw runtime_error("test2() failed");
    else
        cout << "test2() OK\n";
}


void test3()
{
    cout << "test3()\n";

    const ChromosomeArmInfo& arm_info = arm_infos[Chr2L];
    BiallelicVariants variants("test_files_allele_frequencies/2L.biallelic_variants.bin");
    Population_ChromosomePairs population("test_files_allele_frequencies/population_gen2_pop1.txt");
    Population_ChromosomePairs founders("test_files_allele_frequencies/1.founders.pop");

    calculate_allele_frequencies(arm_info, variants, population, founders);
}


void test()
{
    test1();
    test2();
    test3();
}


void calculate_allele_frequencies_all(const Population& population,
                                      const Population& founders,
                                      const char* dirname_dgrp)
{
    for (const ChromosomeArmInfo* arm_info=arm_infos; arm_info!=arm_infos+5; ++arm_info)
    {
        string filename = arm_info->arm + string(".biallelic_variants.bin");
        bfs::path fullpath = bfs::path(dirname_dgrp) / filename;
        if (!bfs::exists(fullpath)) continue;
        BiallelicVariants variants(fullpath.string());
        calculate_allele_frequencies(*arm_info, variants, population, founders);
    }
}


int main(int argc, char* argv[])
{
    try
    {
        //test(); return 0;

        if (argc<4)
        {
            cout << "Usage: dgrp_derived_population_allele_frequencies <final.pop> <founders.pop> <dgrp_dir>\n";
            return 0;
        }

        const char* filename_final = argv[1];
        const char* filename_founders = argv[2];
        const char* dirname_dgrp = argv[3];

        Population_ChromosomePairs population(filename_final);
        Population_ChromosomePairs founders(filename_founders);

        calculate_allele_frequencies_all(population, founders, dirname_dgrp);

        return 0;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch(...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


