//
// population_allele_freqs.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2013 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "Population_ChromosomePairs.hpp" // forqs
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "boost/tokenizer.hpp"
#include "boost/lambda/lambda.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <numeric>


using namespace std;
namespace bfs = boost::filesystem;
using namespace boost::lambda; // for _1


const size_t haplotype_count_ = 162;
                

vector<string> tokenize_commas(const string& s)
{
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
    tokenizer tokens(s, sep);
    vector<string> result;
    copy(tokens.begin(), tokens.end(), back_inserter(result));
    return result;
}


vector<float> get_counts(unsigned int position, 
                         const vector<char>& bases, 
                         const Population& population,
                         unsigned int ids_per_haplotype, 
                         size_t chromosome_pair_index)
{
    enum {A, C, G, T};
    vector<float> counts(4); // ACGT

    //cout << "position: " << position << endl;

    for (const ChromosomePairRangeIterator it=population.begin(); it!=population.end(); ++it)
    {
        const ChromosomePair* chromosome_pair = it->begin() + chromosome_pair_index;

        unsigned int ids[2] = {0, 0};

        ids[0] = chromosome_pair->first.find_haplotype_chunk(position)->id / ids_per_haplotype;
        ids[1] = chromosome_pair->second.find_haplotype_chunk(position)->id / ids_per_haplotype;

        for (size_t which=0; which<2; ++which)
        {
            if (ids[which] >= bases.size())
                throw runtime_error("Bad haplotype id.");

            char base = bases[ids[which]];
            //cout << "base: " << ids[which] << " " << base << endl;

            switch(base)
            {
                case 'A': counts[A] += 1; break;
                case 'C': counts[C] += 1; break;
                case 'G': counts[G] += 1; break;
                case 'T': counts[T] += 1; break;
                case 'R': counts[G] += .5; counts[A] += .5; break;
                case 'Y': counts[T] += .5; counts[C] += .5; break;
                case 'K': counts[G] += .5; counts[T] += .5; break;
                case 'M': counts[A] += .5; counts[C] += .5; break;
                case 'S': counts[G] += .5; counts[C] += .5; break;
                case 'W': counts[A] += .5; counts[T] += .5; break;
                case 'N': counts[A] += .25; counts[C] += .25; counts[G] += .25; counts[T] += .25; break;
                default:
                    ostringstream message;
                    message << "Bad base: " << base;
                    throw runtime_error(message.str());
            }
        }
    }

    float total = accumulate(counts.begin(), counts.end(), 0.);
    if (total != population.population_size() * 2)
        throw runtime_error("Unexpected count total.");

    return counts;
}


size_t to_index(char base)
{
    switch(base)
    {
        case 'A': return 0;
        case 'C': return 1;
        case 'G': return 2;
        case 'T': return 3;
        default: throw runtime_error("[to_index] Bad base.");
    }
}


bool is_header_line(const string& buffer)
{
    string first_two = buffer.substr(0,2);

    return (first_two == "2L" ||
            first_two == "2R" ||
            first_two == "3L" ||
            first_two == "3R" ||
            first_two == "X,");
}


void process_snps_file(const string& filename, const Population& population,
                       unsigned int ids_per_haplotype, size_t chromosome_pair_index, size_t position_offset)
{
    ifstream is(filename.c_str());
    if (!is)
        throw runtime_error("Unable to open file " + filename);

    cout << "position allele_frequency\n";

    while(is)
    {
        string buffer;
        getline(is, buffer);
        if (!is) break;
        if (is_header_line(buffer)) continue;

        vector<string> tokens = tokenize_commas(buffer);
        if (tokens.size() != haplotype_count_ + 4)
        {
            copy(tokens.begin(), tokens.end(), ostream_iterator<string>(cout, " "));
            throw runtime_error("Unexpected number of tokens.");
        }

        unsigned int position = atoi(tokens[0].c_str());
        char ref = tokens[1][0];

        vector<char> bases(haplotype_count_);
        transform(tokens.begin()+2, tokens.end()-2, bases.begin(), _1[0]);

        vector<float> counts = get_counts(position + position_offset, bases, population, 
                                          ids_per_haplotype, chromosome_pair_index);

        size_t ref_index = to_index(ref);
        float ref_count = counts[ref_index];

        float alt_count = 0.0;
        for (size_t index=0; index<4; ++index)
            if (index != ref_index && counts[index] > alt_count)
                alt_count = counts[index];

        float allele_frequency = alt_count / (alt_count + ref_count);

        //cout << ref << " " << ref_count << " " << alt_count << endl;
        cout << position << " " << allele_frequency << endl;
    }
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc<5)
        {
            cout << "Usage: population_allele_freqs <dgrp_snps> <forqs_population> <ids_per_haplotype> <chromosome_pair_index> [position_offset]\n";
            cout << endl;
            return 0;
        }

        const char* filename_snps = argv[1];
        const char* filename_population = argv[2];
        unsigned int ids_per_haplotype = atoi(argv[3]);
        size_t chromosome_pair_index = atoi(argv[4]);
        size_t position_offset = 0;
        if (argc > 5) position_offset = int(atof(argv[5]));


        Population_ChromosomePairs population;
        ifstream is(filename_population);
        if (!is)
            throw runtime_error("Unable to open file " + string(filename_population));
        population.read_text(is);
        is.close();

        cerr << "population_size: " << population.population_size() << endl;
        cerr << "ids_per_haplotype: " << ids_per_haplotype << endl;
        cerr << "chromosome_pair_index: " << chromosome_pair_index << endl;
        cerr << "position_offset: " << position_offset << endl;

        process_snps_file(filename_snps, population, ids_per_haplotype, chromosome_pair_index, position_offset);

        return 0;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch(...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


