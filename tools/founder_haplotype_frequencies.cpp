//
// founder_haplotype_frequencies.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2014 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "Population_ChromosomePairs.hpp" // forqs
#include <boost/filesystem.hpp>
#include <iostream>
#include <iterator>


using namespace std;
namespace bfs = boost::filesystem;


vector<unsigned int> get_id_list(const Population& population, 
                                 size_t chromosome_pair_index,
                                 unsigned int position)
{
    vector<unsigned int> result;
    result.reserve(population.population_size() * 2);

    for (const ChromosomePairRangeIterator it=population.begin(); it!=population.end(); ++it)
    {
        const ChromosomePair* chromosome_pair = it->begin() + chromosome_pair_index;

        unsigned int id = chromosome_pair->first.find_haplotype_chunk(position)->id;
        result.push_back(id);

        id = chromosome_pair->second.find_haplotype_chunk(position)->id;
        result.push_back(id);
    }
 
    return result;
}


vector<unsigned int> translate_id_list(const vector<unsigned int>& from,
                                       const vector<unsigned int>& to)
{
    vector<unsigned int> result(from.size());
    unsigned int id_end = to.size();

    vector<unsigned int>::iterator newid=result.begin();
    for (vector<unsigned int>::const_iterator id=from.begin(); id!=from.end(); ++id, ++newid)
    {
        if (*id >= id_end) throw runtime_error("[translate_id_list] Invalid id.");
        *newid = to[*id];
    }
    
    return result;
}


struct ChromosomeArmInfo
{
    const char* arm;
    size_t chromosome_pair_index; // forqs
    unsigned int position_offset; // forqs
    unsigned int chromosome_length;
    unsigned int absolute_offset;
};


enum {ChrX, Chr2L, Chr2R, Chr3L, Chr3R};


ChromosomeArmInfo arm_infos[] =
{
    {"X",   0, 0,           22500000, 0},
    {"2L",  1, 0,           23000000, 22500000},
    {"2R",  1, 25000000,    21100000, 45500000},
    {"3L",  2, 0,           24500000, 66600000},
    {"3R",  2, 25000000,    28000000, 91100000}
};


void calculate_haplotype_frequencies(const Population& population,
                                     const Population& founders)
{
    const unsigned int haplotype_count = 162;
    const unsigned int position_step = 100000;

    cout << "position ";
    for (size_t h=1; h<=haplotype_count; ++h) cout << "haplotype" << h << " ";
    cout << endl;

    for (const ChromosomeArmInfo* arm_info=arm_infos; arm_info!=arm_infos+5; ++arm_info)
    {
        for (unsigned int position=0; position<arm_info->chromosome_length; position+=position_step)
        {
            unsigned int chromosome_position = position + arm_info->position_offset;

            vector<unsigned int> founder_id_list = get_id_list(founders, 
                                                               arm_info->chromosome_pair_index, 
                                                               chromosome_position);

            vector<unsigned int> id_list = get_id_list(population, 
                                                       arm_info->chromosome_pair_index, 
                                                       chromosome_position);

            vector<unsigned int> ids_translated = translate_id_list(id_list, founder_id_list);

            vector<double> haplotype_frequencies(haplotype_count);
            for (vector<unsigned int>::const_iterator it=ids_translated.begin();
                 it != ids_translated.end(); ++it)
                ++haplotype_frequencies.at(*it);

            const double population_size = ids_translated.size();

            for (vector<double>::iterator it=haplotype_frequencies.begin(); 
                 it!=haplotype_frequencies.end(); ++it)
                *it /= population_size;

            unsigned int absolute_position = position + arm_info->absolute_offset;

            // output

            cout << absolute_position << " ";
            copy(haplotype_frequencies.begin(), haplotype_frequencies.end(), 
                 ostream_iterator<double>(cout, " "));
            cout << endl;
        }
    }
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc<3)
        {
            cout << "Usage: founder_haplotype_frequencies <final.pop> <founders.pop>\n";
            return 0;
        }

        const char* filename_final = argv[1];
        const char* filename_founders = argv[2];

        Population_ChromosomePairs population(filename_final);
        Population_ChromosomePairs founders(filename_founders);

        calculate_haplotype_frequencies(population, founders);

        return 0;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
        return 1;
    }
    catch(...)
    {
        cout << "Caught unknown exception.\n";
        return 1;
    }
}


