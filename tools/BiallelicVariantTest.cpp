//
// BiallelicVariantTest.cpp
//
// Created by Darren Kessner with John Novembre
//
// Copyright (c) 2014 Regents of the University of California
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// 
// * Neither UCLA nor the names of its contributors may be used to endorse or
// promote products derived from this software without specific prior
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#include "BiallelicVariant.hpp"
#include "unit.hpp"
#include <iostream>
#include <iterator>
#include <cstring>


using namespace std;


ostream* os_ = 0;
//ostream* os_ = &cout;

void test_BiallelicVariant_bits()
{
    if (os_) *os_ << "test_BiallelicVariant_bits()\n";

    BiallelicVariant variant;

    for (size_t i=0; i<162; i+=2)
        variant.set_bit(i, true);

    if (os_)
    {
        *os_ << "bits:\n";
        for (size_t i=0; i<6; ++i)
            *os_ << hex << variant.bits[i] << " ";
        *os_ << endl;
    }

    for (size_t i=0; i<162; ++i)
        unit_assert(variant.get_bit(i) == (i%2==0));
}


void test_BiallelicVariant_constructor()
{
    os_ = &cout;

    if (os_) *os_ << "test_BiallelicVariant_constructor()\n";

    ostringstream line;
    line << "12345678,A,"; // position, reference allele
    for (size_t i=0; i<10; ++i)
        line << "A,C,G,T,R,Y,K,M,S,W,B,D,H,V,N,"; // 1,0,0,0,1,1,...,1
    line << "A,C,G,T,R,Y,K,M,S,W,B,D,1,"; // bases, Coverage

    BiallelicVariant variant(line.str());

    for (size_t i=0; i<162; ++i)
    {
        if (i%15==1 || i%15==2 || i%15==3)
            unit_assert(variant.get_bit(i) == 1);
        else
            unit_assert(variant.get_bit(i) == 0);
    }
}


struct KnownInfo
{
    int position;
    char reference;
    double allele_frequency;
};


KnownInfo known_infos_[] = // calculate_dgrp_nonref_allele_frequencies.py
{
    {9999887, 'G', 0.111111111111},
    {9999890, 'G', 0.111111111111},
    {9999928, 'A', 0.469135802469},
    {9999929, 'A', 0.462962962963},
    {9999972, 'T', 0.00617283950617},
    {9999988, 'C', 0.0},
    {9999989, 'C', 0.0185185185185},
    {10000016, 'C', 0.469135802469},
    {10000023, 'C', 0.0123456790123},
    {10000029, 'G', 0.00617283950617},
    {10000033, 'G', 0.450617283951},
    {10000089, 'C', 0.487654320988},
    {10000133, 'A', 0.0432098765432},
    {10000135, 'A', 0.438271604938},
    {10000172, 'T', 0.00617283950617},
    {10000234, 'C', 0.530864197531},
    {10000272, 'G', 0.0185185185185},
    {10000273, 'T', 0.154320987654},
    {10000277, 'A', 0.00617283950617},
    {10000278, 'G', 0.037037037037},
    {10000279, 'G', 0.0432098765432},
    {10000281, 'A', 0.0925925925926},
    {10000294, 'T', 0.537037037037},
    {10000315, 'A', 0.00617283950617},
};


void test_BiallelicVariants_read_text()
{
    if (os_) *os_ << "test_BiallelicVariants()\n";

    BiallelicVariants variants;
    variants.read_text("test_files/sparse_snps_2L_10m.txt");
    unit_assert(variants.size() == 24);

    vector<unsigned int> counts(162, 1);

    KnownInfo* known = known_infos_;
    for (BiallelicVariants::const_iterator it=variants.begin(); it!=variants.end(); ++it, ++known)
    {
        //if (os_) *os_ << it->position << " " << it->reference << " " << it->allele_frequency(counts) << endl;
        unit_assert(it->position == known->position);
        unit_assert(it->reference == known->reference);
        unit_assert_equal(it->allele_frequency(counts), known->allele_frequency, 1e-6);
    }
}


void test_BiallelicVariants_read_write_binary()
{
    if (os_) *os_ << "test_BiallelicVariants_read_write_binary()";

    BiallelicVariants variants;
    variants.read_text("test_files/sparse_snps_2L_10m.txt");
    unit_assert(variants.size() == 24);

    const char* filename_temp = "test_files/temp.bin";
    variants.write_binary(filename_temp);

    BiallelicVariants variants2(filename_temp);
    unit_assert(variants2.size() == 24);

    for (size_t i=0; i<24; ++i)
        unit_assert(variants[i] == variants2[i]);
}


void test()
{
    test_BiallelicVariant_bits();
    test_BiallelicVariant_constructor();
    test_BiallelicVariants_read_text();
    test_BiallelicVariants_read_write_binary();
}


int main(int argc, char* argv[])
{
    try
    {
        if (argc>1 && !strcmp(argv[1],"-v")) os_ = &cout;
        test();
        return 0;
    }
    catch(exception& e)
    {
        cerr << e.what() << endl;
        return 1;
    }
    catch(...)
    {
        cerr << "Caught unknown exception.\n";
        return 1;
    }
}


