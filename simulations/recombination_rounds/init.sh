#!/bin/bash
#
# init.sh (recombination_rounds)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="2 5 10 100"
heritabilities=".2 .5 .8"
recombination_rates="1 2 3 4"

filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname qtl_count heritability recombination_rate" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
for recombination_rate in $recombination_rates; do
    dirname=qtl${qtl_count}_h${heritability}_recomb${recombination_rate}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh $dirname
    cp mix.simconfig.template main.config $dirname
    sed "s/QTL_COUNT/$qtl_count/g;\
         s/HERITABILITY/$heritability/g" \
         trait_architecture.config.template \
         > $dirname/trait_architecture.config
    cp up_down_recomb$recombination_rate.simconfig.template $dirname/up_down.simconfig.template
    echo "$dirname $qtl_count $heritability $recombination_rate" >> $filename_parameter_table
done; done; done;


