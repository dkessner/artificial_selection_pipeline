#!/bin/bash
#
# init.sh (length_proportion_selected)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="100"
heritabilities=".2 .5 .8"
#proportions_selected=".05 .1 .2 .3"
proportions_selected=".2 .4 .6 .8"

filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname qtl_count heritability proportion_selected" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
for proportion_selected in $proportions_selected; do
    dirname=qtl${qtl_count}_h${heritability}_prop${proportion_selected}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh $dirname
    cp *.simconfig.template $dirname
    sed "s/QTL_COUNT/$qtl_count/g;\
         s/HERITABILITY/$heritability/g" \
         trait_architecture.config.template \
         > $dirname/trait_architecture.config
    sed "s/PROPORTION_SELECTED/$proportion_selected/g" \
         main.config.template \
         > $dirname/main.config
    echo "$dirname $qtl_count $heritability $proportion_selected" >> $filename_parameter_table
done; done; done;


