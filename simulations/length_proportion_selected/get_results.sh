#!/bin/bash
#
# get_results.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#

source $HOME/.bash_profile

mkdir -p results


for d in $(cat dirnames.txt); do
for gen in gen20 gen40 gen60 gen80; do
for method in single_site single_site_variance region_10kb; do
    filename=results/$d.$gen.$method.average_power
    echo $filename
    calculate_average_power.py $filename 1000 $d/*.processed/*.$gen.$method.power
done; done; done


tar cjvf results.tbz results

