#
# length_proportion_selected_post_processing.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'

dgrp_dir := ~/data/dgrp_snps


%.processed: %.gen20.region_10kb.power %.gen40.region_10kb.power \
             %.gen60.region_10kb.power %.gen80.region_10kb.power
	mkdir $@
	mv $*.*.power $@ 


# pop1: up
# pop2: down


%.gen20.region_10kb.power: %.gen20.D_sorted
	calculate_power.py $*.gen20.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.gen40.region_10kb.power: %.gen40.D_sorted
	calculate_power.py $*.gen40.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.gen60.region_10kb.power: %.gen60.D_sorted
	calculate_power.py $*.gen60.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.gen80.region_10kb.power: %.gen80.D_sorted
	calculate_power.py $*.gen80.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.D_sorted: %.D
	sort -k3 -g -r $*.D > $*.D_sorted

%.D: %.pop1.allele_freqs %.pop2.allele_freqs
	D_2pop.py $*.pop1.allele_freqs $*.pop2.allele_freqs abs > $@

%.gen20.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen20_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.gen20.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen20_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.gen40.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen40_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.gen40.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen40_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.gen60.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen60_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.gen60.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen60_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.gen80.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen80_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.gen80.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_gen80_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@


# preserve intermediate files
#.SECONDARY:


