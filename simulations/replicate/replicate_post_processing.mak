#
# replicate_post_processing.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'


dgrp_dir := ~/data/dgrp_snps


%.processed: %.rep1.power %.rep2.power %.rep3.power %.rep4.power %.rep5.power
	mkdir $@


%.rep1.power: %.rep1.D_sorted
	calculate_power.py $< $*.trait_summary $*.founders.ms \
	     single_site single_site_variance region_10kb region_100kb region_1mb

%.rep2.power: %.rep2.D_sorted
	calculate_power.py $< $*.trait_summary $*.founders.ms \
	     single_site single_site_variance region_10kb region_100kb region_1mb

%.rep3.power: %.rep3.D_sorted
	calculate_power.py $< $*.trait_summary $*.founders.ms \
	     single_site single_site_variance region_10kb region_100kb region_1mb

%.rep4.power: %.rep4.D_sorted
	calculate_power.py $< $*.trait_summary $*.founders.ms \
	     single_site single_site_variance region_10kb region_100kb region_1mb

%.rep5.power: %.rep5.D_sorted
	calculate_power.py $< $*.trait_summary $*.founders.ms \
	     single_site single_site_variance region_10kb region_100kb region_1mb


%.D_sorted: %.D
	sort -k3 -g -r $*.D > $*.D_sorted


%.rep1.D %.rep2.D %.rep3.D %.rep4.D %.rep5.D: \
    %.pop1.allele_freqs %.pop2.allele_freqs \
    %.pop3.allele_freqs %.pop4.allele_freqs \
    %.pop5.allele_freqs %.pop6.allele_freqs \
    %.pop7.allele_freqs %.pop8.allele_freqs \
    %.pop9.allele_freqs %.pop10.allele_freqs
	D_multi.py $*.pop1.allele_freqs $*.pop2.allele_freqs > $*.rep1.D
	D_multi.py $*.pop1.allele_freqs $*.pop2.allele_freqs \
	    $*.pop3.allele_freqs $*.pop4.allele_freqs > $*.rep2.D
	D_multi.py $*.pop1.allele_freqs $*.pop2.allele_freqs \
	    $*.pop3.allele_freqs $*.pop4.allele_freqs \
	    $*.pop5.allele_freqs $*.pop6.allele_freqs > $*.rep3.D
	D_multi.py $*.pop1.allele_freqs $*.pop2.allele_freqs \
	    $*.pop3.allele_freqs $*.pop4.allele_freqs \
	    $*.pop5.allele_freqs $*.pop6.allele_freqs \
	    $*.pop7.allele_freqs $*.pop8.allele_freqs > $*.rep4.D
	D_multi.py $*.pop1.allele_freqs $*.pop2.allele_freqs \
	    $*.pop3.allele_freqs $*.pop4.allele_freqs \
	    $*.pop5.allele_freqs $*.pop6.allele_freqs \
	    $*.pop7.allele_freqs $*.pop8.allele_freqs \
	    $*.pop9.allele_freqs $*.pop10.allele_freqs > $*.rep5.D


%.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop3.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop3.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop4.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop4.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop5.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop5.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop6.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop6.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop7.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop7.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop8.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop8.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop9.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop9.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop10.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop10.txt \
	    $*.founders.pop $(dgrp_dir) > $@


# preserve intermediate files
#.SECONDARY:


