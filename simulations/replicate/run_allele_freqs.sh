#!/bin/bash
#
# run_allele_freqs.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


source $HOME/.bash_profile


if [ "$SGE_TASK_ID" == "" ]
then
    echo '$SGE_TASK_ID not set'
    exit 1
fi

id=$SGE_TASK_ID

# parallelize by population:
#   task id 1-10:  run 1, population 1-10
#   task id 11-20:  run 2, population 1-10
#   ...

run=$((($id - 1) / 10 + 1))
population=$(($id - (($run - 1) * 10)))

target=$run.pop$population.allele_freqs
echo Making $target
make -f replicate_post_processing.mak $target


