#!/bin/bash
#
# submit_run_replicate_post_processing.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ $# -ne 2 ]
then
    echo "Usage: submit_run_replicate_post_processing.sh <taskid_low> <taskid_high>"
    exit 1
fi

taskid_low=$1
taskid_high=$2

echo Ready to submit job array: $taskid_low $taskid_high
read

mkdir -p output
jobarray.q -jl $taskid_low -jh $taskid_high -ji 1 -t 4 -d 2000 -o output run_replicate_post_processing.sh

