#!/bin/bash
#
# submit_run_allele_freqs.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ $# -ne 1 ]
then
    echo "Usage: submit_run_allele_freqs.sh <replicate_count>"
    exit 1
fi

replicate_count=$1
taskid_high=$(($replicate_count * 10))
taskid_low=1

echo Ready to submit job array: $replicate_count replicates, $taskid_high task ids
read

mkdir -p output
jobarray.q -jl $taskid_low -jh $taskid_high -ji 1 -t 12 -d 2000 -o output run_allele_freqs.sh $*

