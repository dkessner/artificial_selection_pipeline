#!/bin/bash
#
# init.sh (selcoeff_meta)
#
# Darren Kessner
# Novembre Lab, UCLA
#


selection_coefficients=".2 .3 .4"
initial_allele_frequencies=".2"

filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt

echo "dirname selection_coefficient initial_allele_frequency" > $filename_parameter_table

for selection_coefficient in $selection_coefficients; do
for initial_allele_frequency in $initial_allele_frequencies; do
    dirname=s${selection_coefficient}_p${initial_allele_frequency}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak $dirname
    cp run_10_sims.sh $dirname
    cp *.config $dirname
    sed "s/INITIAL_ALLELE_FREQUENCY/$initial_allele_frequency/g;\
         s/SELECTION_COEFFICIENT/$selection_coefficient/g"\
         selcoeff.simconfig.template.template \
         > $dirname/selcoeff.simconfig.template
    echo "$dirname $selection_coefficient $initial_allele_frequency" >> $filename_parameter_table
done; done;


