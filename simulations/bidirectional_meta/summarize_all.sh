#!/bin/bash
#
# summarize_all.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


for d in $(cat dirnames.txt) 
do
    echo $d 
    pushd $d
    ./summarize.sh 
    popd
done


tar cjvf summary.tbz qtl*/roc*.pdf qtl*/*.average_power*


