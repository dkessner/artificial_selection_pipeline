#!/bin/bash
#
# init.sh (interference)
#
# Darren Kessner
# Novembre Lab, UCLA
#


heritabilities=".2 .5 .8"


filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt

echo "dirname heritability environmental_variance" > $filename_parameter_table

for heritability in $heritabilities; do
    dirname=h${heritability}

    if [ $heritability == .2 ]; then environmental_variance=2;
    elif [ $heritability == .5 ]; then environmental_variance=.5; 
    elif [ $heritability == .8 ]; then environmental_variance=.125; 
    else echo "bad" && exit 1; fi

    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh $dirname
    cp *.simconfig.template $dirname
    cp main.config trait_architecture.config $dirname
    cp 0.founders.pop 0.founders.ms 0.locus_list 0.trait_summary $dirname
    sed "s/ENVIRONMENTAL_VARIANCE/$environmental_variance/g"\
        0.trait.template > $dirname/0.trait
    echo "$dirname $heritability $environmental_variance" >> $filename_parameter_table
done


