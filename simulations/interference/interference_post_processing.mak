#
# interference_post_processing.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'

dgrp_dir := ~/data/dgrp_snps


%.processed: %.D_QTL
	make -f 2pop_post_processing.mak $@
	mv $*.D_QTL $@


%.D_QTL: %.main.out
	allele_frequencies_final_summary_to_D_QTL.py \
	    $*.main.out/allele_frequencies_final_summary.txt \
	    > $@


# preserve intermediate files
.SECONDARY:


