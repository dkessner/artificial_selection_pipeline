#
# 2pop_post_processing.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'

dgrp_dir := ~/data/dgrp_snps


%.processed: %.region_1mb.power
	mkdir $@
	mv $*.*.power $@


# pop1: up
# pop2: down


%.region_1mb.power: %.D_sorted
	calculate_power.py $*.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb region_100kb region_1mb

%.D_sorted: %.D
	sort -k3 -g -r $*.D > $*.D_sorted

%.D: %.pop1.allele_freqs %.pop2.allele_freqs
	D_2pop.py $*.pop1.allele_freqs $*.pop2.allele_freqs abs > $@

%.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@


# preserve intermediate files
.SECONDARY:


