#!/bin/bash
#
# init.sh (manhattan)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="10"
heritabilities=".5"
configs="1 2"


filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname qtl_count heritability config" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
for config in $configs; do
    dirname=qtl${qtl_count}_h${heritability}_config${config}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.sh $dirname
    cp *.mak $dirname
    cp up_down.simconfig.template.$config $dirname/up_down.simconfig.template
    cp main.config trait_architecture.config $dirname
    cp 0.founders.pop 0.founders.ms 0.locus_list 0.trait 0.trait_summary $dirname
    echo "$dirname $qtl_count $heritability $config" >> $filename_parameter_table
done; done; done


