#!/bin/bash
#
# run_founder_haplotype_frequencies_all.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ $# != 2 ]
then
    echo "Usage: run_founder_haplotype_frequencies_all.sh indir outdir"
    exit 1
fi

indir=$1
outdir=$2

generation_count=$(( $(ls $indir/population_gen*_pop1.txt | wc -l) - 1))
population_count=$(ls $indir/population_final*.txt | wc -l)

if [ -e $outdir ]
then
    echo $outdir already exists.
    exit 1
fi

echo indir: $indir
echo outdir: $outdir
echo generation_count: $generation_count
echo population_count: $population_count

mkdir $outdir

for pop in $(seq 1 $population_count); do
for gen in $(seq 0 $generation_count); do
    filename_out=$outdir/pop${pop}_gen$gen.hapfreqs 
    echo $filename_out
    founder_haplotype_frequencies $indir/population_gen${gen}_pop$pop.txt 0.founders.pop > $filename_out
done; done


