#!/bin/bash
#
# init.sh (truncation_motivation)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="100"
heritabilities=".8"
focal_qtl_effect_sizes=".1 .3 .5"
focal_qtl_allele_frequencies=".2"

filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt

echo "dirname qtl_count heritability focal_qtl_effect_size focal_qtl_allele_frequency" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
for focal_qtl_effect_size in $focal_qtl_effect_sizes; do
for focal_qtl_allele_frequency in $focal_qtl_allele_frequencies; do
    dirname=qtl${qtl_count}_h${heritability}_effect${focal_qtl_effect_size}_p${focal_qtl_allele_frequency}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh $dirname
    cp *.simconfig.template $dirname
    cp main.config $dirname
    sed "s/QTL_COUNT/$qtl_count/g;\
         s/HERITABILITY/$heritability/g;\
         s/FOCAL_QTL_EFFECT_SIZE/$focal_qtl_effect_size/g;\
         s/FOCAL_QTL_ALLELE_FREQUENCY/$focal_qtl_allele_frequency/g"\
         trait_architecture.config.template \
         > $dirname/trait_architecture.config
    echo "$dirname $qtl_count $heritability $focal_qtl_effect_size $focal_qtl_allele_frequency" >> $filename_parameter_table
done; done; done; done


