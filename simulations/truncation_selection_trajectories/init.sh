#!/bin/bash

architecture_config=architecture_config.txt

template=truncation_template.txt
replacement=replacement.txt

generate_architecture.py $architecture_config

replicate_count=$(ls trait*.summary.txt | wc -l)
generate_config_files.py $template $replacement $replicate_count

