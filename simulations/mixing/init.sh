#!/bin/bash
#
# init.sh (mixing)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="2 5 10 100"
heritabilities=".2 .5 .8"
mix_generation_counts="20 40 60 80"


filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname qtl_count heritability" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
for mix_generation_count in $mix_generation_counts; do
    dirname=qtl${qtl_count}_h${heritability}_mix${mix_generation_count}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh $dirname
    cp *.simconfig.template $dirname
    cp main.config $dirname
    sed "s/QTL_COUNT/$qtl_count/g;\
         s/HERITABILITY/$heritability/g;\
         s/MIX_GENERATION_COUNT/$mix_generation_count/g"\
         trait_architecture.config.template \
         > $dirname/trait_architecture.config
    echo "$dirname $qtl_count $heritability $mix_generation_count" >> $filename_parameter_table
done; done; done;


