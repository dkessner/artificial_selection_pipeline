#!/bin/bash
#
# run_plot_trajectories.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ $# -ne 1 ]
then
    echo "Usage: run_plot_trajectories.sh blah.out"
    exit 1
fi

outdir=$1

filename_allele_freqs=$outdir/allele_frequencies_chr1_pos500000.txt
filename_deterministic=$outdir/deterministic_trajectories_allele_frequencies.txt
title=$(grep $outdir parameter_table.txt | cut -d' ' -f5,7)
ylabel="Allele frequency"
filename_output=${outdir/.out/.trajectories.pdf}

plot_trajectories.R $filename_allele_freqs $filename_deterministic "$title" "$ylabel" $filename_output

