#
# architecture_config.txt
#


trait_replicate_count 1
population_replicate_count 100

trait_generator fixed_qtl_count
population_generator homozygous_founders

founder_line_count 162
individuals_per_founder_line 5

# X
# 2L: 0-23.1mb, 2R: 25-46.2mb
# 3L: 0-24.6mb, 3R: 25-52.9mb

chromosome_lengths 2.25e7 4.62e7 5.3e7
trait_name ipi
qtl_count 10
neutral_count 20
total_variance .01
heritability 0.5

