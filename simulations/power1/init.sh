#!/bin/bash

architecture_config=architecture_config.txt

template=turner_experiment_template.txt
replacement=replacement.txt
replicate_count=100

generate_architecture.py $architecture_config

generate_config_files.py $template $replacement $replicate_count

