#!/bin/bash
#
# plot_average_power_all.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


for method in single_site single_site_variance region_10kb region_100kb region_1mb; do
    filename=roc.$method.pdf
    echo $filename
    plot_average_power.R up_down.$method.average_power up_neutral.$method.average_power $filename
    for zoom in 1 2 3 4 5 6
    do
        filename_zoom=roc.$method.zoom$zoom.pdf
        plot_average_power.R up_down.$method.average_power.zoom$zoom \
            up_neutral.$method.average_power.zoom$zoom $filename_zoom
    done
done


