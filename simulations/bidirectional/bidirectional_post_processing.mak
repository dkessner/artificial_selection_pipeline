#
# bidirectional_post_processing.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'

dgrp_dir := ~/data/dgrp_snps


%.processed: %.up_neutral.D_sorted %.up_down.D_sorted
	mkdir $@
	pushd $@ && calculate_power.py ../$*.up_neutral.D_sorted ../$*.trait_summary ../$*.founders.ms \
	    single_site single_site_variance region_10kb region_100kb region_1mb
	pushd $@ && calculate_power.py ../$*.up_down.D_sorted ../$*.trait_summary ../$*.founders.ms \
	    single_site single_site_variance region_10kb region_100kb region_1mb

# pop1: neutral
# pop2: up
# pop3: down


%.D_sorted: %.D
	sort -k3 -g -r $*.D > $*.D_sorted

%.up_neutral.D: %.pop1.allele_freqs %.pop2.allele_freqs
	D_2pop.py $*.pop2.allele_freqs $*.pop1.allele_freqs abs > $@

%.up_down.D: %.pop2.allele_freqs %.pop3.allele_freqs
	D_2pop.py $*.pop2.allele_freqs $*.pop3.allele_freqs abs > $@

%.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.pop3.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop3.txt \
	    $*.founders.pop $(dgrp_dir) > $@


# preserve intermediate files
.SECONDARY:


