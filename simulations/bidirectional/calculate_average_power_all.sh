#!/bin/bash
#
# calculate_average_power_all.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


for comparison in up_down up_neutral; do
for method in single_site single_site_variance region_10kb region_100kb region_1mb; do
    filename=$comparison.$method.average_power
    echo $filename
    calculate_average_power.py $filename 1000 *.$comparison.$method.power
done; done


