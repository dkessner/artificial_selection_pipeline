#!/bin/bash
#
# init.sh (population_size)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="2 5 10 100"
heritabilities=".2 .5 .8"
population_sizes="500 1000 2000 5000"

filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname qtl_count heritability population_size" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
for population_size in $population_sizes; do
    founder_population_size=$((2 * $population_size))
    dirname=qtl${qtl_count}_h${heritability}_N${population_size}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh $dirname
    cp mix.simconfig.template up_down.simconfig.template $dirname
    sed "s/QTL_COUNT/$qtl_count/g;\
         s/HERITABILITY/$heritability/g;\
         s/FOUNDER_POPULATION_SIZE/$founder_population_size/g" \
         trait_architecture.config.template \
         > $dirname/trait_architecture.config
    sed "s/POPULATION_SIZE/$population_size/g" \
         main.config.template \
         > $dirname/main.config
    echo "$dirname $qtl_count $heritability $population_size" >> $filename_parameter_table
done; done; done;


