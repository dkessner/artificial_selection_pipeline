#
# harp_estimation_gen_post_processing.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'

dgrp_dir := ~/data/dgrp_snps


%.processed: %.true.region_10kb.power  %.pileup.region_10kb.power \
    %.harp50.region_10kb.power %.harp100.region_10kb.power \
    %.harp150.region_10kb.power %.harp200.region_10kb.power
	mkdir $@
	mv $*.*.power $@


# pop1: up
# pop2: down


%.true.region_10kb.power: %.true.D_sorted
	calculate_power.py $*.true.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.pileup.region_10kb.power: %.pileup.D_sorted
	calculate_power.py $*.pileup.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp50.region_10kb.power: %.harp50.D_sorted
	calculate_power.py $*.harp50.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp100.region_10kb.power: %.harp100.D_sorted
	calculate_power.py $*.harp100.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp150.region_10kb.power: %.harp150.D_sorted
	calculate_power.py $*.harp150.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp200.region_10kb.power: %.harp200.D_sorted
	calculate_power.py $*.harp200.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp250.region_10kb.power: %.harp250.D_sorted
	calculate_power.py $*.harp250.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp300.region_10kb.power: %.harp300.D_sorted
	calculate_power.py $*.harp300.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp350.region_10kb.power: %.harp350.D_sorted
	calculate_power.py $*.harp350.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp400.region_10kb.power: %.harp400.D_sorted
	calculate_power.py $*.harp400.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.D_sorted: %.D
	sort -k3 -g -r $*.D > $*.D_sorted

%.D: %.pop1.allele_freqs %.pop2.allele_freqs
	D_2pop.py $*.pop1.allele_freqs $*.pop2.allele_freqs abs > $@

%.harp50.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen50.empirical_error_distribution.txt > $@

%.harp50.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen50.empirical_error_distribution.txt > $@

%.harp100.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen100.empirical_error_distribution.txt > $@

%.harp100.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen100.empirical_error_distribution.txt > $@

%.harp150.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen150.empirical_error_distribution.txt > $@

%.harp150.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen150.empirical_error_distribution.txt > $@

%.harp200.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen200.empirical_error_distribution.txt > $@

%.harp200.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen200.empirical_error_distribution.txt > $@

%.harp250.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen250.empirical_error_distribution.txt > $@

%.harp250.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen250.empirical_error_distribution.txt > $@

%.harp300.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen300.empirical_error_distribution.txt > $@

%.harp300.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen300.empirical_error_distribution.txt > $@

%.harp350.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen350.empirical_error_distribution.txt > $@

%.harp350.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen350.empirical_error_distribution.txt > $@

%.harp400.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen400.empirical_error_distribution.txt > $@

%.harp400.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< \
	    empirical_error_distributions/gen400.empirical_error_distribution.txt > $@

%.pileup.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py pileup $< > $@

%.pileup.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py pileup $< > $@

%.true.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.true.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@


# preserve intermediate files
#.SECONDARY:


