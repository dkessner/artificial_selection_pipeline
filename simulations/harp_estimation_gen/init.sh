#!/bin/bash
#
# init.sh (harp_estimation_gen)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="10 100"
heritabilities=".5"


filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname qtl_count heritability" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
    dirname=qtl${qtl_count}_h${heritability}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh $dirname
    cp *.simconfig.template $dirname
    cp main.config $dirname
    cp -r empirical_error_distributions $dirname
    sed "s/QTL_COUNT/$qtl_count/g;\
         s/HERITABILITY/$heritability/g"\
         trait_architecture.config.template \
         > $dirname/trait_architecture.config
    echo "$dirname $qtl_count $heritability" >> $filename_parameter_table
done; done


