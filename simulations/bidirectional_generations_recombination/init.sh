#!/bin/bash
#
# init.sh (bidirectional_generations_recombination)
#
# Darren Kessner
# Novembre Lab, UCLA
#


generation_counts="20 50 100"
recombination_rates="1 2 4"


filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname generation_count recombination_rate" > $filename_parameter_table


for generation_count in $generation_counts; do
for recombination_rate in $recombination_rates; do
    dirname=gencount${generation_count}_recomb${recombination_rate}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.mak *.sh *.config *.R $dirname
    cp mix.simconfig.template $dirname
    sed "s/GENERATION_COUNT/$generation_count/g;\
         s/RECOMBINATION_RATE/$recombination_rate/g"\
         neutral_up_down.simconfig.template.template \
         > $dirname/neutral_up_down.simconfig.template
    echo "$dirname $generation_count $recombination_rate" >> $filename_parameter_table
done; done;


