#!/bin/bash
#
# summarize_all.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


for d in $(cat dirnames.txt) 
do
    echo $d 
    pushd $d
    ./summarize.sh 
    popd
done


tar cjvf summary.tbz gencount*/roc*.pdf gencount*/*.average_power


