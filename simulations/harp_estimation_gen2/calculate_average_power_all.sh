#!/bin/bash
#
# calculate_average_power_all.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


results_dir=results


if [ -d $results_dir ]
then
    echo "Directory $results_dir already exists."
    exit 1
fi


mkdir $results_dir


for d in $(cat dirnames.txt); do
for method in single_site single_site_variance region_10kb; do
for estimate in true pileup harp ; do
    filename=$results_dir/$d.$estimate.$method.average_power
    echo $filename
    calculate_average_power.py $filename 1000 $d/*.processed/*.$estimate.$method.power
done; done; done


tar cjvf results.tbz $results_dir


