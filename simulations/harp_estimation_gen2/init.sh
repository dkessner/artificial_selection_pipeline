#!/bin/bash
#
# init.sh (harp_estimation_gen)
#
# Darren Kessner
# Novembre Lab, UCLA
#


qtl_counts="10 100"
heritabilities=".5"
configs="1 2 3"


filename_dirnames=dirnames.txt
filename_parameter_table=parameter_table.txt


echo "dirname qtl_count heritability" > $filename_parameter_table

for qtl_count in $qtl_counts; do
for heritability in $heritabilities; do
for config in $configs; do
    dirname=qtl${qtl_count}_h${heritability}_config${config}
    echo $dirname | tee -a $filename_dirnames
    mkdir $dirname
    cp *.sh $dirname
    cp main.mak trait_architecture.mak $dirname
    cp harp_estimation_gen_post_processing.$config.mak $dirname/harp_estimation_gen_post_processing.mak
    cp mix.simconfig.template $dirname
    cp up_down.simconfig.template.$config $dirname/up_down.simconfig.template
    cp main.config $dirname
    cp -r empirical_error_distributions $dirname
    sed "s/QTL_COUNT/$qtl_count/g;\
         s/HERITABILITY/$heritability/g"\
         trait_architecture.config.template.$config \
         > $dirname/trait_architecture.config
    echo "$dirname $qtl_count $heritability $config" >> $filename_parameter_table
done; done; done


