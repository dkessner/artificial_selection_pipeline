#
# harp_estimation_gen_post_processing.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'

dgrp_dir := ~/data/dgrp_snps


%.processed: %.true.region_10kb.power  %.pileup.region_10kb.power %.harp.region_10kb.power 
	mkdir $@
	mv $*.*.power $@


# pop1: up
# pop2: down


%.true.region_10kb.power: %.true.D_sorted
	calculate_power.py $*.true.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.pileup.region_10kb.power: %.pileup.D_sorted
	calculate_power.py $*.pileup.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.harp.region_10kb.power: %.harp.D_sorted
	calculate_power.py $*.harp.D_sorted $*.trait_summary $*.founders.ms \
		single_site single_site_variance region_10kb

%.D_sorted: %.D
	sort -k3 -g -r $*.D > $*.D_sorted

%.D: %.pop1.allele_freqs %.pop2.allele_freqs
	D_2pop.py $*.pop1.allele_freqs $*.pop2.allele_freqs abs > $@

%.harp.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py harp $< > $@

%.harp.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py harp $< > $@

%.pileup.pop1.allele_freqs: %.true.pop1.allele_freqs
	add_allele_freq_errors_simple.py pileup $< > $@

%.pileup.pop2.allele_freqs: %.true.pop2.allele_freqs
	add_allele_freq_errors_simple.py pileup $< > $@

%.true.pop1.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop1.txt \
	    $*.founders.pop $(dgrp_dir) > $@

%.true.pop2.allele_freqs: %.main.out
	dgrp_derived_population_allele_frequencies $*.main.out/population_final_pop2.txt \
	    $*.founders.pop $(dgrp_dir) > $@


# preserve intermediate files
#.SECONDARY:


