#
# song_pipeline.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# $* stem of implicit rule match
# $@ target
# $< first prerequisite


dummy:
	@echo 'Usage: make -f song_pipeline.mak <targets>'

# summaries

qtl_summaries := $(patsubst %.simconfig,%.qtl_summary,$(wildcard *.simconfig))

summaries: qtl_summary.txt \
           qtl_summary_errors.txt

qtl_summary_errors.txt: qtl_summary.txt
	add_allele_freq_errors.py $< > $@

qtl_summary.txt: $(qtl_summaries)
	merge_tables.sh *.qtl_summary | sort -n > qtl_summary.txt

# DGRP allele frequency calculations for D null distribution
# note: requires DGRP SNP data in ~/data/dgrp_snps

%.dgrp_cdf_D: %.dgrp_allele_freqs_with_errors
	calculate_ecdf_D.R $< $@

%.dgrp_allele_freqs_with_errors: %.dgrp_allele_freqs
	add_allele_freq_errors.py $< > $@

%.dgrp_allele_freqs: %.out
	calculate_dgrp_allele_freqs.py $*.out ~/data/dgrp_snps > $@

# forqs and post-processing

%.qtl_summary: %.simconfig %.out
	merge_qtl_data.py $* $*.simconfig $*.out/allele_frequencies_final_summary.txt > $@

%.out: %.simconfig
	forqs $<

clean:
	rm -r *.out
	rm *qtl_summary*
	rm *.dgrp*

# preserve intermediate files
.SECONDARY:


