#
# trait_architecture_dummy.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


%.trait %.founders.ms %.locus_list:
	touch $*.trait $*.founders.ms $*.locus_list


clean:
	rm -f *.trait *.founders.ms *.locus_list

