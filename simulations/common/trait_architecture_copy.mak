#
# trait_architecture_copy.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


%.trait %.founders.ms %.locus_list:
	cp 0.trait $*.trait
	cp 0.trait_summary $*.trait_summary
	cp 0.founders.ms $*.founders.ms 
	cp 0.founders.pop $*.founders.pop 
	cp 0.locus_list $*.locus_list

