#
# main.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


config_file := main.config


ifeq ($(shell test -f $(config_file); echo $$?), 1)
$(error $(config_file) not found.)
endif


# parameters from config file

trait_architecture_makefile := $(shell get_parameter_value_from_config.py trait_architecture_makefile $(config_file))
trait_architecture_config := $(shell get_parameter_value_from_config.py trait_architecture_config $(config_file))

main_simconfig_template := $(shell get_parameter_value_from_config.py main_simconfig_template $(config_file))

proportion_selected := $(shell get_parameter_value_from_config.py proportion_selected $(config_file))
population_size := $(shell get_parameter_value_from_config.py population_size $(config_file))

post_processing_makefile := $(shell get_parameter_value_from_config.py post_processing_makefile $(config_file))

# parameters from trait architecture config

focal_qtl_chromosome_pair_index := $(shell get_parameter_value_from_config.py focal_qtl_chromosome_pair_index $(trait_architecture_config))
focal_qtl_position := $(shell get_parameter_value_from_config.py focal_qtl_position $(trait_architecture_config))

# derived variables

focal_qtl_chromosome := $(shell echo $$(($(focal_qtl_chromosome_pair_index) + 1)))
filename_allele_frequencies := allele_frequencies_chr$(focal_qtl_chromosome)_pos$(focal_qtl_position).txt

# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make <targets>'


validate:
	@echo trait_architecture_makefile: $(trait_architecture_makefile)
	@test $(trait_architecture_makefile) != UNKNOWN
	@echo trait_architecture_config: $(trait_architecture_config)
	@test $(trait_architecture_config) != UNKNOWN
	@echo main_simconfig_template: $(main_simconfig_template)
	@test $(main_simconfig_template) != UNKNOWN
	@echo proportion_selected: $(proportion_selected)
	@test $(proportion_selected) != UNKNOWN
	@echo population_size: $(population_size)
	@test $(population_size) != UNKNOWN
	@echo focal_qtl_chromosome_pair_index : $(focal_qtl_chromosome_pair_index)
	@test $(focal_qtl_chromosome_pair_index) != UNKNOWN
	@echo focal_qtl_position : $(focal_qtl_position)
	@test $(focal_qtl_position) != UNKNOWN
	@echo post_processing_makefile: $(post_processing_makefile)
	@test $(post_processing_makefile) != UNKNOWN


summary: realized_s focal_qtl_allele_frequencies.txt


focal_qtl_allele_frequencies.txt:
	paste *.main.out/$(filename_allele_frequencies) > $@


realized_s:
	analyze_selection.R $@ 0 *.main.out/$(filename_allele_frequencies)


%.processed: %.main.out
	make -f $(post_processing_makefile) $@


%.main.out: %.main.simconfig
	forqs $< > /dev/null


%.main.simconfig: $(main_simconfig_template) %.founders.ms %.locus_list %.trait 
	seed=$$(od -vAn -N4 -tu4 < /dev/urandom);\
	sed "s/FILENAME/$@/g;\
	     s/OUTPUT_DIRECTORY/$*.main.out/g;\
	     s/SEED/$$seed/g;\
	     s/POPULATION_SIZE/$(population_size)/g;\
	     s/PROPORTION_SELECTED/$(proportion_selected)/g;\
	     s/MSFILE/$*.founders.ms/g;\
	     s/LOCUS_LIST/$*.locus_list/g;\
	     s/TRAIT/$*.trait/g"\
	     $(main_simconfig_template) > $@


%.trait %.founders.ms %.locus_list: $(trait_architecture_makefile) validate
	make -f $(trait_architecture_makefile) $*.trait


clean:
	rm -rf *.main.out
	rm -f *.pdf


# preserve intermediate files
.SECONDARY:


