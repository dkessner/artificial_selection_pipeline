#
# trait_architecture.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


config_file := trait_architecture.config


ifeq ($(shell test -f $(config_file); echo $$?), 1)
$(error $(config_file) not found.)
endif


# parameters from config file

mix_simconfig_template_filename := $(shell get_parameter_value_from_config.py mix_simconfig_template_filename $(config_file))
mix_population_size := $(shell get_parameter_value_from_config.py mix_population_size $(config_file))
mix_generation_count := $(shell get_parameter_value_from_config.py mix_generation_count $(config_file))

dirname_dgrp_snps := $(shell get_parameter_value_from_config.py dirname_dgrp_snps $(config_file))

focal_qtl_chromosome_pair_index := $(shell get_parameter_value_from_config.py focal_qtl_chromosome_pair_index $(config_file))
focal_qtl_position := $(shell get_parameter_value_from_config.py focal_qtl_position $(config_file))
focal_qtl_chromosome_arm := $(shell get_parameter_value_from_config.py focal_qtl_chromosome_arm $(config_file))
focal_qtl_dgrp_line_number := $(shell get_parameter_value_from_config.py focal_qtl_dgrp_line_number $(config_file))
focal_qtl_allele_frequency := $(shell get_parameter_value_from_config.py focal_qtl_allele_frequency $(config_file))
focal_qtl_effect_size := $(shell get_parameter_value_from_config.py focal_qtl_effect_size $(config_file))

founder_population_size := $(shell get_parameter_value_from_config.py founder_population_size $(config_file))

qtl_count := $(shell get_parameter_value_from_config.py qtl_count $(config_file))
total_variance := $(shell get_parameter_value_from_config.py total_variance $(config_file))
heritability := $(shell get_parameter_value_from_config.py heritability $(config_file))



# make automatic variables:
# $* stem of implicit rule match [st(em) -> st(ar)]
# $@ target [ta(rget) -> at]
# $< first prerequisite [first < everything else]


usage:
	@echo 'Usage: make -f trait_architecture.mak <targets>'


validate:
	@echo mix_simconfig_template_filename: $(mix_simconfig_template_filename)
	@test $(mix_simconfig_template_filename) != UNKNOWN
	@echo mix_population_size: $(mix_population_size)
	@test $(mix_population_size) != UNKNOWN
	@echo mix_generation_count: $(mix_generation_count)
	@test $(mix_generation_count) != UNKNOWN
	@echo dirname_dgrp_snps: $(dirname_dgrp_snps)
	@test $(dirname_dgrp_snps) != UNKNOWN
	@echo focal_qtl_chromosome_pair_index: $(focal_qtl_chromosome_pair_index)
	@test $(focal_qtl_chromosome_pair_index) != UNKNOWN
	@echo focal_qtl_position: $(focal_qtl_position)
	@test $(focal_qtl_position) != UNKNOWN
	@echo focal_qtl_chromosome_arm: $(focal_qtl_chromosome_arm)
	@test $(focal_qtl_chromosome_arm) != UNKNOWN
	@echo focal_qtl_dgrp_line_number: $(focal_qtl_dgrp_line_number)
	@test $(focal_qtl_dgrp_line_number) != UNKNOWN
	@echo focal_qtl_allele_frequency: $(focal_qtl_allele_frequency)
	@test $(focal_qtl_allele_frequency) != UNKNOWN
	@echo focal_qtl_effect_size: $(focal_qtl_effect_size)
	@test $(focal_qtl_effect_size) != UNKNOWN
	@echo founder_population_size: $(founder_population_size)
	@test $(founder_population_size) != UNKNOWN
	@echo qtl_count: $(qtl_count)
	@test $(qtl_count) != UNKNOWN
	@echo total_variance: $(total_variance)
	@test $(total_variance) != UNKNOWN
	@echo heritability: $(heritability)
	@test $(heritability) != UNKNOWN


%.trait: %.create_trait.config 
	@echo Creating trait: $@
	@create_trait.py $*.create_trait.config > $*.create_trait.stdout


%.create_trait.config: %.founders.ms %.locus_list
	@echo "#" >> $@
	@echo "# $@" >> $@
	@echo "#" >> $@
	@echo >> $@
	@echo "filename_output $*.trait" >> $@
	@echo "filename_output_summary $*.trait_summary" >> $@
	@echo "filename_founder_haplotypes $*.founders.ms" >> $@
	@echo "filename_locus_list $*.locus_list" >> $@
	@echo >> $@
	@echo "founder_population_size $(founder_population_size)" >> $@
	@echo "total_variance $(total_variance)" >> $@
	@echo "heritability $(heritability)" >> $@
	@echo >> $@
	@echo "focal_qtl_chromosome_pair_index $(focal_qtl_chromosome_pair_index)" >> $@
	@echo "focal_qtl_position $(focal_qtl_position)" >> $@
	@echo "focal_qtl_allele_frequency $(focal_qtl_allele_frequency)" >> $@
	@echo "focal_qtl_effect_size $(focal_qtl_effect_size)" >> $@


%.founders.pop %.founders.ms: %.forqs_focal_subset.config focal_qtl.dgrp.ms
	forqs_focal_subset $<


%.forqs_focal_subset.config: %.mix.out
	@echo "#" >> $@
	@echo "# $@" >> $@
	@echo "#" >> $@
	@echo >> $@
	@echo "filename_full_population $</population_final_pop1.txt" >> $@
	@echo "filename_full_ms $</variants_final_pop1.txt" >> $@
	@echo "filename_original_snps focal_qtl.dgrp.ms" >> $@
	@echo "filename_subset_population $*.founders.pop" >> $@
	@echo "filename_subset_ms $*.founders.ms" >> $@
	@echo "population_size $(founder_population_size)" >> $@
	@echo "focal_locus $(focal_qtl_chromosome_pair_index) $(focal_qtl_position)" >> $@
	@echo "focal_allele_frequency $(focal_qtl_allele_frequency)" >> $@


%.mix.out: %.mix.simconfig
	forqs $<


%.mix.simconfig: $(mix_simconfig_template_filename) %.dgrp.ms %.locus_list
	seed=$$(od -vAn -N4 -tu4 < /dev/urandom);\
	sed "s/FILENAME/$*.mix.simconfig/g;\
	     s/OUTPUT_DIRECTORY/$*.mix.out/g;\
	     s/SEED/$$seed/g;\
	     s/POPULATION_SIZE/$(mix_population_size)/g;\
	     s/GENERATION_COUNT/$(mix_generation_count)/g;\
	     s/MSFILE/$*.dgrp.ms/g;\
	     s/LOCUS_LIST/$*.locus_list/g"\
	     $(mix_simconfig_template_filename) > $@


%.dgrp.ms %.locus_list %.dgrp_allele_frequencies: %.dgrp_line_numbers
	dgrp_lines_to_ms.py $< $(dirname_dgrp_snps) $*
	

valid_line_numbers := ~/data/dgrp_snps/nonzero_allele_frequency_line_numbers.npz

%.dgrp_line_numbers: validate
	@if [ $(focal_qtl_dgrp_line_number) -ne 0 ]; then \
	    random_dgrp_positions.py count=$(qtl_count) line_numbers=$(valid_line_numbers) \
		include=$(focal_qtl_chromosome_arm):$(focal_qtl_dgrp_line_number) > $@ ;\
	else \
	    random_dgrp_positions.py count=$(qtl_count) line_numbers=$(valid_line_numbers) > $@ ;\
	fi


focal_qtl.dgrp.ms: focal_qtl.dgrp_line_numbers
	dgrp_lines_to_ms.py $< $(dirname_dgrp_snps) focal_qtl


focal_qtl.dgrp_line_numbers: validate
	@if [ $(focal_qtl_dgrp_line_number) -ne 0 ]; then \
	echo "# arm line_number" >> $@ ;\
	echo "$(focal_qtl_chromosome_arm) $(focal_qtl_dgrp_line_number)" >> $@;\
	fi
	touch $@


clean:
	rm -f forqs.seed 
	rm -rf *.simconfig *.out
	rm -f focal_qtl.dgrp.ms focal_qtl.dgrp_line_numbers
	rm -f *.forqs_focal_subset.config
	rm -f *.founders.pop *.founders.ms
	rm -f *.dgrp_line_numbers *.dgrp.ms *.locus_list *.dgrp_allele_frequencies
	rm -f *.create_trait.config *.create_trait.stdout *.trait


.SECONDARY:

