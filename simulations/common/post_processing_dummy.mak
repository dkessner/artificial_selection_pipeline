#
# post_processing_dummy.mak
#
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


%.processed: %.main.out
	mkdir $@


