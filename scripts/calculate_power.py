#!/usr/bin/env python
#
# calculate_power.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import os
import bisect
import unittest
import numpy as np
from genome_region import GenomeRegion
from math import *


class QTL:
    def __init__(self, chromosome_pair_index, position, effect_size = 0):
        self.chromosome_pair_index = chromosome_pair_index
        self.position = position
        self.effect_size = effect_size
    def __str__(self):
        return str(self.chromosome_pair_index) + " " + \
               str(self.position) + " " + str(self.effect_size)
    def __hash__(self): # for set
        return hash((self.chromosome_pair_index, self.position))
    def __cmp__(self, other): # for sorting
        return cmp((self.chromosome_pair_index, self.position), 
                   (other.chromosome_pair_index, other.position))
    def is_near(self, locus, radius):
        return locus.chromosome_pair_index == self.chromosome_pair_index and \
               abs(locus.position - self.position) < radius


class QTL_list(list):
    def __init__(self, filename = ""):
        list.__init__(self)
        if filename: self.parse_trait_summary(filename)
        assert(self == sorted(self))
    def parse_trait_summary(self, filename):
        with open(filename) as f:
            f.readline()
            for line in f:
                tokens = line.split()
                assert(len(tokens) == 3)
                self.append(QTL(int(tokens[0]), int(tokens[1]), float(tokens[2])))
    def effect_sizes(self):
        return np.array([qtl.effect_size for qtl in self])
    def __contains__(self, qtl):
        index = bisect.bisect_left(self, qtl)
        return (index != len(self) and qtl==self[index])


class Test_QTL_list(unittest.TestCase):
    def test_contains(self):
        qtls = QTL_list()
        qtls.append(QTL(0,12345,0))
        qtls.append(QTL(0,12346,0))
        qtls.append(QTL(1,12346,0))
        for qtl in qtls: 
            print(qtl)
            assert(qtl in qtls)
        assert(QTL(1,12347, .3) not in qtls)
        assert(QTL(0,1234, .3) not in qtls)


def calculate_genetic_value(effect_sizes, haplotype1, haplotype2):
    return sum(effect_sizes*haplotype1 + effect_sizes*haplotype2)


def calculate_genetic_values(effect_sizes, snps):
    haplotype_count, locus_count = snps.shape
    population_size = haplotype_count/2
    genetic_values = np.zeros(population_size)
    for i in range(population_size):
        haplotype1 = snps[i*2, :]
        haplotype2 = snps[i*2 + 1, :]
        genetic_values[i] = calculate_genetic_value(effect_sizes, haplotype1, haplotype2)
    return genetic_values


class Haplotypes:

    def __init__(self, filename_founders_ms, qtl_count):
        self.qtl_count = qtl_count
        self.parse_founder_haplotypes(filename_founders_ms)
        print(self.haplotype_count, "haplotypes parsed.")

    def parse_founder_haplotypes(self, filename):
        haplotypes = []
        for line in open(filename):
            if line.startswith('#'): continue
            if line.startswith('segsites:'):
                segsites = int(line.split()[-1])
                assert(segsites == self.qtl_count)
            else: 
                haplotypes.append(line.strip())
        self.haplotype_count = len(haplotypes)
        # store snp values in 2-D array: (h,l) indexes haplotype, locus
        self.snps = np.zeros((self.haplotype_count, self.qtl_count), np.int)
        for h, haplotype in enumerate(haplotypes):
            for l, snp_value in enumerate(haplotype):
                self.snps[h,l] = int(snp_value)
        # print("allele freqs:")
        # for l in range(self.qtl_count):
        #     print(sum(self.snps[:,l]) / float(self.haplotype_count))

    def genetic_variance(self, effect_sizes):
        genetic_values = calculate_genetic_values(effect_sizes, self.snps)
        return np.var(genetic_values)


class PowerCalculator:

    def __init__(self, qtls, haplotypes, filestem, neutral_count_total):

        self.qtls = qtls
        self.haplotypes = haplotypes
        self.neutral_count_total = neutral_count_total

        self.filename = filestem + "." + self.name() + ".power"
        if os.path.exists(self.filename): raise Exception(self.filename + " already exists.")

        self.qtl_count_total = float(len(qtls))
        self.effect_sizes = qtls.effect_sizes()
        self.genetic_variance = haplotypes.genetic_variance(self.effect_sizes)

        self.fp_rate = 0.0
        self.power = 0.0

    def open_file(self):
        self.file = open(self.filename, "w")
        print("fp_rate power", file=self.file)

    def print_current_fp_rate_power(self):
        print(self.fp_rate, self.power, file=self.file)


class PowerCalculator_SingleSite(PowerCalculator):

    @classmethod
    def name(self): return "single_site"

    def __init__(self, *args):
        PowerCalculator.__init__(self, *args)
        self.qtl_count = 0.0
        self.neutral_count = 0.0

    def update(self, chromosome_pair_index, position, D):
        if QTL(chromosome_pair_index, position) in self.qtls:
            self.qtl_count += 1
            self.power = self.qtl_count / self.qtl_count_total
        else:
            self.neutral_count += 1
            self.fp_rate = self.neutral_count / self.neutral_count_total


class PowerCalculator_SingleSiteVariance(PowerCalculator):

    @classmethod
    def name(self): return "single_site_variance"

    def __init__(self, *args):
        PowerCalculator.__init__(self, *args)
        self.neutral_count = 0.0
        self.qtls_detected = np.zeros(self.qtl_count_total)

    def update(self, chromosome_pair_index, position, D):
        locus = QTL(chromosome_pair_index, position)
        if locus in self.qtls:
            qtl_index = self.qtls.index(locus)
            self.qtls_detected[qtl_index] = 1
            variance_explained = \
                self.haplotypes.genetic_variance(self.qtls_detected * self.effect_sizes)
            self.power = variance_explained / self.genetic_variance
        else:
            self.neutral_count += 1
            self.fp_rate = self.neutral_count / self.neutral_count_total


class PowerCalculator_Region(PowerCalculator):

    def __init__(self, radius, *args):
        PowerCalculator.__init__(self, *args)
        self.radius = radius

        self.qtl_region = GenomeRegion()
        for qtl in self.qtls: 
            self.qtl_region.insert_locus_region(qtl.chromosome_pair_index, qtl.position, self.radius)

        self.detected_region = GenomeRegion()
        self.detected_region.insert_dgrp_unused()
        for qtl in self.qtls: 
            self.detected_region.insert_locus_region(qtl.chromosome_pair_index, qtl.position, self.radius)

        self.detected_region_initial_size = len(self.detected_region)
        self.neutral_region_size = \
            float(self.detected_region.genome_size - self.detected_region_initial_size)

        #print("genome size:", self.detected_region.genome_size)
        #print("initial size:", self.detected_region_initial_size)

        self.qtls_detected = np.zeros(self.qtl_count_total)
        self.qtl_count = 0

    def update(self, chromosome_pair_index, position, D):
        locus = QTL(chromosome_pair_index, position)
        if locus in self.qtl_region:
            for index, qtl in enumerate(self.qtls):
                if qtl.is_near(locus, self.radius):
                    self.qtls_detected[index] = 1
            # optimization: only re-calculate variance if we detect a new qtl
            if self.qtl_count != sum(self.qtls_detected):
                self.qtl_count = sum(self.qtls_detected)
                variance_explained = \
                    self.haplotypes.genetic_variance(self.qtls_detected * self.effect_sizes)
                self.power = variance_explained / self.genetic_variance
        else:
            self.detected_region.insert_locus_region(chromosome_pair_index, position, self.radius)
            self.fp_rate = (len(self.detected_region) - self.detected_region_initial_size) / \
                self.neutral_region_size
            #print("current size:", len(self.detected_region))
        if self.fp_rate > 1:
            raise Exception("fp_rate: " + str(self.fp_rate))


class PowerCalculator_Region_10kb(PowerCalculator_Region):
    @classmethod
    def name(self): return "region_10kb"
    def __init__(self, *args): 
        PowerCalculator_Region.__init__(self, 10000, *args)


class PowerCalculator_Region_100kb(PowerCalculator_Region):
    @classmethod
    def name(self): return "region_100kb"
    def __init__(self, *args): 
        PowerCalculator_Region.__init__(self, 100000, *args)


class PowerCalculator_Region_1mb(PowerCalculator_Region):
    @classmethod
    def name(self): return "region_1mb"
    def __init__(self, *args): 
        PowerCalculator_Region.__init__(self, 1000000, *args)


name_power_calculator_map_ = {power_calculator.name():power_calculator for power_calculator in \
    [PowerCalculator_SingleSite, 
     PowerCalculator_SingleSiteVariance,
     PowerCalculator_Region_10kb,
     PowerCalculator_Region_100kb,
     PowerCalculator_Region_1mb]
}


def instantiate_power_calculators(methods, qtls, haplotypes, filestem):
    dgrp_snp_count = 5222888
    neutral_count_total = dgrp_snp_count - len(qtls)
    power_calculators = []
    for method in methods:
        if method in name_power_calculator_map_:
            power_calculators.append(
                name_power_calculator_map_[method]
                    (qtls, haplotypes, filestem, neutral_count_total))
        else:
            print("Warning: Ignoring unknown power calculation method", method)
    return power_calculators


def process_D_sorted(filename_D_sorted, power_calculators):
    for power_calculator in power_calculators:
        power_calculator.open_file()
    D_current = None
    for line in open(filename_D_sorted):
        tokens = line.split()
        chromosome_pair_index, position, D = int(tokens[0]), int(tokens[1]), float(tokens[2])
        if D_current != D:
            for power_calculator in power_calculators:
                power_calculator.print_current_fp_rate_power()
        D_current = D
        for power_calculator in power_calculators:
            power_calculator.update(chromosome_pair_index, position, D)
    for power_calculator in power_calculators:
        power_calculator.print_current_fp_rate_power()


def main():
    if len(sys.argv) < 5:
        print("Usage: calculate_power.py <.D_sorted> <.trait_summary> <.founders.ms> <method1> [method2 ...]\n")
        sys.exit(1)

    filename_D_sorted = sys.argv[1]
    filename_trait_summary = sys.argv[2]
    filename_founders_ms = sys.argv[3]
    methods = set(sys.argv[4:])

    qtls = QTL_list(filename_trait_summary)
    #for qtl in qtls: print(qtl)

    haplotypes = Haplotypes(filename_founders_ms, len(qtls))    
    effect_sizes = qtls.effect_sizes()
    print("genetic variance:", haplotypes.genetic_variance(effect_sizes))

    filestem = filename_D_sorted.replace(".D_sorted", "")
    power_calculators = instantiate_power_calculators(methods, qtls, haplotypes, filestem)

    process_D_sorted(filename_D_sorted, power_calculators)


if __name__ == '__main__':
    main()

