#!/bin/bash

prefix=artificial_selection_pipeline
filename=$prefix.tbz
echo Creating archive $filename
git archive --prefix=$prefix/ master | bzip2 > $filename
echo Ready to copy $filename to hoffman2.
scp $filename dkessner@hoffman2.idre.ucla.edu:~/dev

