#!/usr/bin/env python
#
# estimate_allele_freqs_from_hapfreqs.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import numpy as np
import unittest


haplotype_count_ = 162
chromosome_arms_ = ['2L', '2R', '3L', '3R', 'X']


def read_freqs(filename):
    hapfreqs_map = {}
    for line in open(filename):
        tokens = line.split()
        chromosome_arm, position_begin, position_end = tokens[0], int(tokens[1]), int(tokens[2])
        freqs = np.array([float(f) for f in tokens[3:]])
        assert(chromosome_arm in chromosome_arms_ and len(freqs) == haplotype_count_)
        hapfreqs_map[(position_begin, position_end)] = freqs
    return hapfreqs_map


def get_hapfreqs(position, hapfreqs_map):
    best_window = None
    best_distance = float("inf")
    for window in hapfreqs_map:
        position_begin, position_end = window
        if position>=position_begin and position<position_end:
            window_center = (position_begin + position_end)/2
            distance = abs(position - window_center)
            if distance < best_distance:
                best_distance = distance
                best_window = window
    if best_window is None:
        raise Exception("Window not found for position " + str(position))
    return best_window, hapfreqs_map[best_window]



# switch(base)
# {
#     case 'A': counts[A] += 1; break;
#     case 'C': counts[C] += 1; break;
#     case 'G': counts[G] += 1; break;
#     case 'T': counts[T] += 1; break;
#     case 'R': counts[G] += .5; counts[A] += .5; break;
#     case 'Y': counts[T] += .5; counts[C] += .5; break;
#     case 'K': counts[G] += .5; counts[T] += .5; break;
#     case 'M': counts[A] += .5; counts[C] += .5; break;
#     case 'S': counts[G] += .5; counts[C] += .5; break;
#     case 'W': counts[A] += .5; counts[T] += .5; break;
#     case 'N': counts[A] += .25; counts[C] += .25; counts[G] += .25; counts[T] += .25; break;
#     default:
#         ostringstream message;
#         message << "Bad base: " << base;
#         throw runtime_error(message.str());
# }


def to_index(base):
    if base == 'A': return 0
    if base == 'C': return 1
    if base == 'G': return 2
    if base == 'T': return 3
    raise Exception("No index for base " + base)


def process_snp_file(filename, hapfreqs_map):
    with open(filename) as f:
        first_two = f.readline()[:2]
        assert(first_two in chromosome_arms_)
        print("position allele_frequency")
        for line in f:
            tokens = line.split(',')
            position = int(tokens[0])
            ref = tokens[1]
            bases = tokens[2:-2]
            assert(len(bases) == haplotype_count_)
            window, hapfreqs = get_hapfreqs(position, hapfreqs_map)
            A,C,G,T = range(4) 
            counts = np.zeros(4)
            for base, f in zip(bases, hapfreqs):
                if base == 'A': 
                    counts[A] += f
                elif base == 'C':
                    counts[C] += f
                elif base == 'G':
                    counts[G] += f
                elif base == 'T':
                    counts[T] += f
                elif base == 'R':
                    counts[G] += f/2
                    counts[A] += f/2
                elif base == 'Y':
                    counts[T] += f/2
                    counts[C] += f/2
                elif base == 'K':
                    counts[G] += f/2
                    counts[T] += f/2
                elif base == 'M':
                    counts[A] += f/2
                    counts[C] += f/2
                elif base == 'S':
                    counts[G] += f/2
                    counts[C] += f/2
                elif base == 'W':
                    counts[A] += f/2
                    counts[T] += f/2
                elif base == 'N':
                    counts[A] += f/4
                    counts[C] += f/4
                    counts[G] += f/4
                    counts[T] += f/4
                else:
                    raise Exception("Bad base.")

            #print("counts: ", counts, sum(counts))

            ref_index = to_index(ref)
            ref_count = counts[ref_index]
            
            alt_count = 0.
            for index in range(4):
                if index != ref_index and counts[index]>alt_count:
                    alt_count = counts[index]

            allele_freq = alt_count/(alt_count + ref_count)

            print(position, allele_freq)


class Test(unittest.TestCase):
    def test_get_hapfreqs(self):
        hapfreqs_map = read_freqs("test_estimate_allele_freqs_from_hapfreqs/1.freqs")
        self.assertEqual(len(hapfreqs_map), 4)
        window, hapfreqs = get_hapfreqs(1000000, hapfreqs_map)
        self.assertEqual(window, (1000000,1200000))
        window, hapfreqs = get_hapfreqs(1350000, hapfreqs_map)
        self.assertEqual(window, (1200000,1400000))
        window, hapfreqs = get_hapfreqs(1350001, hapfreqs_map)
        self.assertEqual(window, (1300000,1500000))
        caught = False
        try:
            window, hapfreqs = get_hapfreqs(1000, hapfreqs_map)
        except Exception as e:
            caught = True
        caught = False
        try:
            window, hapfreqs = get_hapfreqs(1500001, hapfreqs_map)
        except Exception as e:
            caught = True
        self.assertEqual(caught, True)
        

def main():
    if len(sys.argv) != 3:
        print("Usage: estimate_from_hapfreqs.py <hapfreqs> <dgrp_snps>\n")
        print()
        print("Output (pos ref allele_freq):")
        print("    10000016 C 0.67975865")
        sys.exit(1)

    filename_freqs = sys.argv[1]
    filename_snps = sys.argv[2]
    
    hapfreqs_map = read_freqs(filename_freqs)
    process_snp_file(filename_snps, hapfreqs_map)


if __name__ == '__main__':
    main()

