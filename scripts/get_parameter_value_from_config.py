#!/usr/bin/env python
#
# get_parameter_value_from_config.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys


def main():

    if len(sys.argv) != 3:
        print("Usage: get_parameter_value_from_config.py parameter_name config_file\n")
        sys.exit(1)

    parameter_name = sys.argv[1]
    config_file = sys.argv[2]

    for line in open(config_file):
        tokens = line.strip().split()
        if len(tokens) > 0 and tokens[0] == parameter_name:
            print(' '.join(tokens[1:]))
            sys.exit(0)
    
    print("UNKNOWN")

if __name__ == '__main__':
    main()

