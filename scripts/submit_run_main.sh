#!/bin/bash
#
# submit_run_main.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ $# -ne 1 ]
then
    echo "Usage: submit_run_main.sh <replicate_count>"
    exit 1
fi

taskid_high=$1
taskid_low=1

echo Ready to submit job array: $taskid_low $taskid_high
read

mkdir -p output
jobarray.q -jl $taskid_low -jh $taskid_high -ji 1 -t 4 -d 1000 -o output run_main.sh $*

