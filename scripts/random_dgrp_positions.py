#!/usr/bin/env python
#
# random_dgrp_positions.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import random
import numpy as np


chromosome_arms_ = ["dummy", "X", "2L", "2R", "3L", "3R"]


class RandomPositionGenerator:

    def __init__(self, valid_line_numbers):
        for arm in chromosome_arms_[1:]:
            if "arm_"+arm not in valid_line_numbers:
                print("Invalid line number data from npz", file=sys.stderr)
                sys.exit(1)
        self.valid_line_numbers = valid_line_numbers
        self.array_lengths = [0] + \
            [len(valid_line_numbers["arm_"+arm]) for arm in chromosome_arms_[1:]]
        self.array_lengths_cumulative = np.cumsum(self.array_lengths)
        self.total_array_length = self.array_lengths_cumulative[-1]
        # print("array_lengths:", self.array_lengths)
        # print("array_lengths_cumulative:", self.array_lengths_cumulative)
        # print("total_array_length:", self.total_array_length)

    def random_position(self):
        roll = random.randint(1, self.total_array_length-1)
        # print("roll:", roll)
        arm_index = np.searchsorted(self.array_lengths_cumulative, roll)
        # print("arm_index:", arm_index)
        assert(arm_index >=1 and arm_index <= 5)
        line_number_index = roll - self.array_lengths_cumulative[arm_index-1]
        # print("line_number_index:", line_number_index)
        assert(line_number_index < self.array_lengths[arm_index])
        arm = chromosome_arms_[arm_index]
        line_number = self.valid_line_numbers["arm_"+arm][line_number_index]
        # print(arm, line_number)
        return arm, line_number


def main():

    count = 0
    valid_line_numbers = None
    positions = set()

    for arg in sys.argv[1:]:
        name, value = arg.split('=')
        if name == "count":
            count = int(value)
        elif name == "line_numbers":
            if not os.path.isfile(value):
                print("Unable to find file", value, file=sys.stderr)
                sys.exit(1)
            valid_line_numbers = np.load(value)
            # dict: "arm_X" -> np.array of line numbers, "arm_2L" -> ...
        elif name == "include":
            tokens = value.split(":")
            assert(len(tokens) == 2)
            arm, line_number = tokens[0], int(tokens[1])
            positions.add((arm, line_number))
        elif name == "seed":
            random.seed(int(value))

    if count == 0 or valid_line_numbers is None:
        print("Usage: random_dgrp_positions.py name=value [...]")
        print()
        print("Required parameters:")
        print("  count=<int>")
        print("  line_numbers=/path/to/line_numbers.npz   # .npz numpy array of valid line numbers for each arm")
        print()
        print("Optional parameters:")
        print("  include=arm:line_number        # set specific position (multiple allowed)")
        print("  seed=<int>                     # set seed for regression testing")
        print()
        print("Picks <count> random DGRP positions, where <count> includes the")
        print("optional include=arm:line_number entries listed on the command line.")
        print()
        print("Output format:")
        print("# arm line_number")
        print("2L 1234567")
        print("...")
        sys.exit(1)

    rpg = RandomPositionGenerator(valid_line_numbers)

    while len(positions) < count:
        arm, line_number = rpg.random_position()
        if (arm, line_number) in positions: continue
        positions.add((arm, line_number))

    print("# arm line_number")
    for arm, line_number in sorted(positions):
        print(arm, line_number)


if __name__ == '__main__':
    main()

