#!/usr/bin/env python
#
# estimate_allele_freqs_from_pileup.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import numpy as np


def parse_snps(filename):
    snps = {} # position -> ref
    with open(filename) as f:
        first_two = f.readline()[:2]
        if not first_two in ['2L', '2R', '3L', '3R', 'X,']:
            raise Exception("Bad snp file format.")
        for line in f:
            tokens = line.split(',')
            position = int(tokens[0])
            ref = tokens[1]
            snps[position] = ref
    return snps


def count_reads(bases):
    counts = {'A':0, 'C':0, 'G':0, 'T':0}
    i = 0
    while i < len(bases):
        base = bases[i]
        if base in counts:
            counts[base] += 1
            i += 1
        elif base=='$':
            i += 1
        elif base=='^': 
            i += 2
        else:
            raise Exception("Unknown character: " + base)
    return counts


def calculate_allele_frequency(counts, ref):
    ref_count = counts[ref]
    alt_count = 0
    for base in counts:
        if base != ref:
            if alt_count < counts[base]:
                alt_count = counts[base]
    return float(alt_count)/(ref_count + alt_count)


def process_pileup(filename, snps):
    print("position allele_frequency")
    for line in open(filename):
        tokens = line.split()
        position = int(tokens[1])
        if position in snps:
            bases = tokens[4]
            counts = count_reads(bases)
            ref = snps[position]
            allele_freq = calculate_allele_frequency(counts, ref)
            print(position, allele_freq)


def main():
    if len(sys.argv) != 3:
        print("Usage: estimate_from_hapfreqs.py <pileup> <snps>\n")
        sys.exit(1)

    filename_pileup = sys.argv[1]
    filename_snps = sys.argv[2]
    
    snps = parse_snps(filename_snps)
    process_pileup(filename_pileup, snps)


if __name__ == '__main__':
    main()

