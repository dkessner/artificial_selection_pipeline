#!/usr/bin/env python
#
# D_multi.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import itertools


def main():

    if len(sys.argv) < 3:
        print("Usage: D_multi.py 1.allele_freqs 2.allele_freqs [...]\n")
        print("Output: token0 token1 abs(D) (D == freq_1 - freq_2 + freq3 - freq_4 ...)\n")
        sys.exit(1)

    filenames = sys.argv[1:]
    
    if len(filenames)%2 != 0:
        print("Odd number of filenames.")
        sys.exit(1)

    for lines in itertools.izip(*[open(filename) for filename in filenames]):
        splits = [line.split() for line in lines] # each split = (chr,pos,freq)
        assert(len(set([split[0] for split in splits])) == 1) # sanity check: same chromosome
        assert(len(set([split[1] for split in splits])) == 1) # sanity check: same position
        allele_freqs = [float(split[2]) for split in splits]
        D = 0
        for i, allele_freq in enumerate(allele_freqs):
            D += allele_freq if i%2==0 else -allele_freq
        D = abs(D) 
        print(splits[0][0], splits[0][1], D)


if __name__ == '__main__':
    main()

