#!/usr/bin/env python
#
# calculate_average_D.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import itertools
import numpy as np


def main():

    if len(sys.argv) < 3:
        print("Usage: calculate_average_D.py 1.D 2.D [...]\n")
        sys.exit(1)

    filenames = sys.argv[1:]
    
    for lines in itertools.izip(*[open(filename) for filename in filenames]):
        splits = [line.split() for line in lines] # each split = (chr,pos,D)
        assert(len(set([split[0] for split in splits])) == 1) # sanity check: same chromosome
        assert(len(set([split[1] for split in splits])) == 1) # sanity check: same position
        Ds = np.array([float(split[2]) for split in splits])
        average_D = np.mean(Ds)
        print(splits[0][0], splits[0][1], average_D)


if __name__ == '__main__':
    main()

