#
# Makefile for script tests
# 
# Darren Kessner
# Novembre Lab, UCLA
#


SHELL = /bin/bash


# $* stem of implicit rule match
# $@ target
# $< first prerequisite


all: haplotype_frequencies_test \
     estimate_allele_freqs_from_hapfreqs_test \
     merge_qtl_data_test \
     add_allele_freq_errors_test \
     add_allele_freq_errors_simple_test \
     merge_tables_test \
     calculate_dgrp_nonref_allele_frequencies_test \
     dgrp_lines_to_ms_test \
     dgrp_lines_to_ms_test_2 \
     create_trait_unit_test \
     create_trait_regression_test \
     get_parameter_value_from_config_test \
     random_dgrp_positions_test \
     allele_frequency_trajectories_test \
     D_2pop_test \
     D_multi_test \
     calculate_power_unit_test \
     calculate_power_regression_test \
     genome_region_unit_test \
     calculate_average_power_unit_test \
     calculate_average_power_regression_test \
     calculate_average_D_test


haplotype_frequencies_test:
	@echo haplotype_frequencies_test
	python -m unittest haplotype_frequencies
	@echo


estimate_allele_freqs_from_hapfreqs_test:
	@echo estimate_allele_freqs_from_hapfreqs_test
	python -m unittest estimate_allele_freqs_from_hapfreqs
	@echo


merge_qtl_data_test:
	@echo merge_qtl_data_test
	@pushd test_merge_qtl_data > /dev/null;\
	../merge_qtl_data.py 1 1.simconfig allele_frequencies_final_summary.txt | diff output.good -
	@echo merge_qtl_data_test OK
	@echo


add_allele_freq_errors_test:
	@echo add_allele_freq_errors_test
	@pushd test_add_allele_freq_errors > /dev/null;\
	../add_allele_freq_errors.py 1.allele_frequencies_final_summary.txt ../empirical_error_distribution.txt 123 | diff 1.good -
	@pushd test_add_allele_freq_errors > /dev/null;\
	../add_allele_freq_errors.py 2.qtl_summary.txt ../empirical_error_distribution.txt 123 | diff 2.good -
	@echo add_allele_freq_errors_test OK
	@echo


add_allele_freq_errors_simple_test:
	@echo add_allele_freq_errors_simple_test
	@pushd test_add_allele_freq_errors_simple > /dev/null;\
	../add_allele_freq_errors_simple.py harp 1.allele_freqs \
		../empirical_error_distribution.txt 123 | diff 1.harp.allele_freqs.good -
	@pushd test_add_allele_freq_errors_simple > /dev/null;\
	../add_allele_freq_errors_simple.py pileup 1.allele_freqs \
		../empirical_error_distribution.txt 123 | diff 1.pileup.allele_freqs.good -
	@echo add_allele_freq_errors_simple_test OK
	@echo


merge_tables_test:
	@echo merge_tables_test
	@pushd test_merge_tables > /dev/null;\
	../merge_tables.sh 1.txt 2.txt 3.txt | diff output.good -
	@echo merge_tables_test OK
	@echo


calculate_dgrp_nonref_allele_frequencies_test:
	@echo calculate_dgrp_nonref_allele_frequencies_test
	@pushd test_calculate_dgrp_nonref_allele_frequencies > /dev/null;\
	    ../calculate_dgrp_nonref_allele_frequencies.py 2L_1m.txt | diff 2L_1m.nonref_allele_frequencies.good -
	@echo calculate_dgrp_nonref_allele_frequencies_test OK
	@echo


dgrp_lines_to_ms_test:
	@echo dgrp_lines_to_ms_test
	@pushd test_dgrp_lines_to_ms > /dev/null;\
	    ../dgrp_lines_to_ms.py line_numbers.txt ~/data/dgrp_snps/ test;\
	    diff test.dgrp.ms good.dgrp.ms &&\
	    diff test.locus_list good.locus_list &&\
	    diff test.dgrp_allele_frequencies good.dgrp_allele_frequencies
	@pushd test_dgrp_lines_to_ms > /dev/null;\
	    rm test.dgrp.ms test.locus_list test.dgrp_allele_frequencies
	@echo dgrp_lines_to_ms_test OK
	@echo


dgrp_lines_to_ms_test_2:
	@echo dgrp_lines_to_ms_test_2
	@pushd test_dgrp_lines_to_ms > /dev/null;\
	    ../dgrp_lines_to_ms.py line_numbers_2.txt ~/data/dgrp_snps/ test_2;\
	    diff test_2.dgrp.ms good_2.dgrp.ms &&\
	    diff test_2.locus_list good_2.locus_list &&\
	    diff test_2.dgrp_allele_frequencies good_2.dgrp_allele_frequencies
	@pushd test_dgrp_lines_to_ms > /dev/null;\
	    rm test_2.dgrp.ms test_2.locus_list test_2.dgrp_allele_frequencies
	@echo dgrp_lines_to_ms_test_2 OK
	@echo


create_trait_unit_test:
	@echo create_trait_unit_test
	python -m unittest create_trait
	@echo


create_trait_regression_test:
	@echo create_trait_regression_test
	@pushd test_create_trait > /dev/null;\
	    ../create_trait.py 1.create_trait.config &&\
	    diff 1.trait 1.trait.good &&\
	    diff 1.trait_summary 1.trait_summary.good
	@rm test_create_trait/1.trait test_create_trait/1.trait_summary
	@echo create_trait_regression_test OK
	@echo


get_parameter_value_from_config_test:
	@echo get_parameter_value_from_config_test
	@test $$(get_parameter_value_from_config.py seed test_get_parameter_value_from_config/config.txt) = 1234
	@test $$(get_parameter_value_from_config.py filename_output test_get_parameter_value_from_config/config.txt) = hello.txt
	@test $$(get_parameter_value_from_config.py blah test_get_parameter_value_from_config/config.txt) = UNKNOWN
	@echo get_parameter_value_from_config_test OK
	@echo


filename_nonzero_line_numbers := ~/data/dgrp_snps/nonzero_allele_frequency_line_numbers.npz

random_dgrp_positions_test:
	@echo random_dgrp_positions_test
	@if [ ! -f $(filename_nonzero_line_numbers) ] ;\
	then \
	    echo "Warning: $(filename_nonzero_line_numbers) needed for test." ;\
	else \
	    random_dgrp_positions.py count=20 line_numbers=$(filename_nonzero_line_numbers) \
		seed=123 include=X:1234 \
		| diff - test_random_dgrp_positions/line_numbers.good ;\
	    echo random_dgrp_positions_test OK ;\
	    echo ;\
	fi


allele_frequency_trajectories_test:
	@echo allele_frequency_trajectories_test
	pushd test_allele_frequency_trajectories; test_allele_frequency_trajectories.R
	@echo allele_frequency_trajectories_test OK
	@echo


D_2pop_test:
	@echo D_2pop_test
	pushd test_D_2pop; ../D_2pop.py pop1.allele_freqs pop2.allele_freqs | diff - output.good
	pushd test_D_2pop; ../D_2pop.py pop1.allele_freqs pop2.allele_freqs abs | diff - output.abs.good
	@echo D_2pop_test OK
	@echo


D_multi_test:
	@echo D_multi_test
	pushd test_D_2pop; ../D_multi.py pop1.allele_freqs pop2.allele_freqs \
	    | diff - output.abs.good
	pushd test_D_2pop; ../D_multi.py pop1.allele_freqs pop2.allele_freqs \
	    pop1.allele_freqs pop2.allele_freqs \
	    | diff - output4.good
	pushd test_D_2pop; ../D_multi.py pop1.allele_freqs pop2.allele_freqs \
	    pop1.allele_freqs pop2.allele_freqs pop1.allele_freqs pop2.allele_freqs  \
	    | diff - output6.good
	@echo D_multi_test OK
	@echo


calculate_power_unit_test:
	@echo calculate_power_unit_test
	python -m unittest calculate_power
	@echo calculate_power_unit_test OK
	@echo


calculate_power_regression_test:
	@echo calculate_power_regression_test
	pushd test_calculate_power && ../calculate_power.py 1.D_sorted 1.trait_summary \
	    1.founders.ms single_site single_site_variance region_10kb region_100kb region_1mb
	pushd test_calculate_power && diff 1.single_site.power 1.single_site.power.good
	pushd test_calculate_power && diff 1.single_site_variance.power 1.single_site_variance.power.good
	pushd test_calculate_power && diff 1.region_10kb.power 1.region_10kb.power.good
	pushd test_calculate_power && diff 1.region_100kb.power 1.region_100kb.power.good
	pushd test_calculate_power && diff 1.region_1mb.power 1.region_1mb.power.good
	rm test_calculate_power/1.*.power
	@echo calculate_power_regression_test OK
	@echo


genome_region_unit_test:
	@echo genome_region_unit_test
	python -m unittest genome_region
	@echo genome_region_unit_test OK
	@echo


calculate_average_power_unit_test:
	@echo calculate_average_power_unit_test
	python -m unittest calculate_average_power
	@echo calculate_average_power_unit_test OK
	@echo


calculate_average_power_regression_test:
	@echo calculate_average_power_regression_test
	pushd test_calculate_average_power && \
	    ../calculate_average_power.py test.average_power 50 *.power && \
	    diff test.average_power good.average_power &&\
	    diff test.average_power.zoom1 good.average_power.zoom1 &&\
	    diff test.average_power.zoom2 good.average_power.zoom2 &&\
	    diff test.average_power.zoom3 good.average_power.zoom3 &&\
	    diff test.average_power.zoom4 good.average_power.zoom4 &&\
	    rm test.average_power*
	@echo calculate_average_power_regression_test OK
	@echo

calculate_average_D_test:
	@echo calculate_average_D_test
	pushd test_calculate_average_D && ../calculate_average_D.py test0/* | diff - 0.good
	pushd test_calculate_average_D && ../calculate_average_D.py test1/* | diff - 1.good
	@echo calculate_average_D_test OK
	@echo



