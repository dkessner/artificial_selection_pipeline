#!/usr/bin/env python
#
# write_nonzero_allele_frequency_line_numbers_npz.py
#
# Darren Kessner
# Novembre Lab, UCLA
#

from __future__ import print_function
import sys
import os
import numpy as np


def main():

    filename_output = "nonzero_allele_frequency_line_numbers.npz"
    if os.path.exists(filename_output):
        print("Output file", filename_output, "already exists.")
        sys.exit(1)

    arrays = {}
    chromosome_arms = ['X', '2L', '2R', '3L', '3R']

    for arm in chromosome_arms:
        filename_nonref_freqs = arm + '_nonref_allele_frequencies.txt'
        if not os.path.exists(filename_nonref_freqs):
            print(filename_nonref_freqs, "not found.")
            print("Run calculate_dgrp_nonref_allele_frequencies_all.sh")
            sys.exit(1)
        a = np.loadtxt(filename_nonref_freqs, skiprows=1, usecols=(0,2))
        a_nonzero = a[:,1].nonzero()[0] + 2 # +1 for indexing, +1 for header line
        arrays[arm] = a_nonzero
        print(arm, len(a_nonzero))
    
    np.savez(filename_output, arm_X=arrays['X'],
             arm_2L=arrays['2L'], arm_2R=arrays['2R'],
             arm_3L=arrays['3L'], arm_3R=arrays['3R'])


if __name__ == '__main__':
    main()

