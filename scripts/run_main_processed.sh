#!/bin/bash
#
# run_main_processed.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


source $HOME/.bash_profile


if [ "$SGE_TASK_ID" == "" ]
then
    echo '$SGE_TASK_ID not set'
    exit 1
fi

run=$SGE_TASK_ID

target=$run.processed
echo Making $target
make -f main.mak $target


