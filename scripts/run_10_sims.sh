#!/bin/bash
#
# run_10sims.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


source $HOME/.bash_profile


if [ "$SGE_TASK_ID" == "" ]
then
    echo '$SGE_TASK_ID not set'
    exit 1
fi

id=$SGE_TASK_ID

low=$((($id-1) * 10 + 1))
high=$(($id * 10))

echo low: $low
echo high: $high

for run in $(seq $low $high) 
do
    target=$run.main.out
    echo Making $target
    make -f main.mak $target
done

