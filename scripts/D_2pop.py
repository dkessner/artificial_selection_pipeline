#!/usr/bin/env python
#
# D_2pop.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import itertools


def main():

    if len(sys.argv) < 3:
        print("Usage: D_2pop.py 1.allele_freqs 2.allele_freqs [abs]\n")
        print("Output: token0 token1 D (= allele_freq_1 - allele_freq_2)\n")
        sys.exit(1)

    filename_pop1 = sys.argv[1]
    filename_pop2 = sys.argv[2]

    do_abs = False
    if len(sys.argv) == 4:
        if sys.argv[3] == 'abs': do_abs = True
        else: print("Warning: ignoring argument", sys.argv[3])

    for line_pop1, line_pop2 in itertools.izip(open(filename_pop1), open(filename_pop2)):
        chr_1, position_1, frequency_1 = line_pop1.split()
        chr_2, position_2, frequency_2 = line_pop2.split()
        assert(chr_1 == chr_2 and position_1 == position_2)
        D = float(frequency_1) - float(frequency_2)
        if do_abs: D = abs(D)
        print(chr_1, position_1, D)


if __name__ == '__main__':
    main()

