#!/usr/bin/env python
#
# haplotype_frequencies.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import unittest
import sys
import os
import numpy as np


class HaplotypeFrequencies:
    
    # parses forqs haplotype frequency output for 4 populations, 3 chromosomes
    # access by chromosome arm (X, 2L, 2R, 3L, 3R), position, population

    def __init__(self, dirname):
        self.hapfreqs = {}
        for chromosome in range(1,4):       # 1-based chromosome
            for population in range(1,5):   # 1-based population
                self.read_haplotype_frequencies(dirname, chromosome, population)

    def read_haplotype_frequencies(self, dirname, chromosome, population):
        filename = "haplotype_frequencies_chr" + str(chromosome) + \
                "_final_pop" + str(population) + ".txt"
        path_to_filename = os.path.join(dirname, filename)
        if not os.path.exists(path_to_filename):
            raise Exception("[HaplotypeFrequencies] Unable to find file" + path_to_filename)
        for line in open(path_to_filename):
            if line.startswith('#'): continue
            tokens = line.strip().split()
            position = int(tokens[0])
            hapfreqs = np.array([float(token) for token in tokens[1:]])
            assert(len(hapfreqs) == 162)
            self.hapfreqs[(chromosome, position, population)] = hapfreqs

    def get_haplotype_frequencies(self, chromosome_arm, position, population):

        # chromosome_arm: {X, 2L, 2R, 3L, 3R}
        # position: position on arm
        # population: in {1,2,3,4}

        if population not in range(1,5):
            raise Exception("[HaplotypeFrequencies] Population must be in {1,2,3,4}.")
        if chromosome_arm == "X":
            chromosome = 1
        elif chromosome_arm == "2L":
            chromosome = 2
        elif chromosome_arm == "2R":
            chromosome = 2
            position += 25000000
        elif chromosome_arm == "3L":
            chromosome = 3
        elif chromosome_arm == "3R":
            chromosome = 3
            position += 25000000
        else:
            raise Exception("[HaplotypeFrequencies] Chromosome arm must be in {X, 2L, 2R, 3L, 3R}.")
        window_size = 100000 # hard-coded
        window_position = position / window_size * window_size
        return chromosome, position, self.hapfreqs[(chromosome, window_position, population)]


class Test(unittest.TestCase):
    def test_haplotype_frequencies(self):
        hapfreqs = HaplotypeFrequencies("test_haplotype_frequencies")
        self.assertEqual(len(hapfreqs.hapfreqs), 4868)

        chromosome, position, f = hapfreqs.get_haplotype_frequencies("X", 123, 1)
        self.assertEqual(chromosome, 1)
        self.assertEqual(position, 123)
        for i in range(8):
            self.assertEqual(f[i], 0)
        epsilon = 1e-7
        self.assertAlmostEqual(f[8], .0060423, delta=epsilon)
        self.assertAlmostEqual(f[11], .0800604, delta=epsilon)
        self.assertAlmostEqual(f[16], .253776, delta=epsilon)

        chromosome, position, f = hapfreqs.get_haplotype_frequencies("3R", 27700001, 4)
        self.assertEqual(chromosome, 3)
        self.assertEqual(position, 52700001)
        self.assertAlmostEqual(f[0], 0, delta=epsilon)
        self.assertAlmostEqual(f[1], .0483384, delta=epsilon)
        self.assertAlmostEqual(f[13], .261329, delta=epsilon)
        self.assertAlmostEqual(f[143], .0151057, delta=epsilon)
        self.assertAlmostEqual(f[161], .154079, delta=epsilon)

        chromosome, position, f = hapfreqs.get_haplotype_frequencies("3R", 27700001, 3)
        self.assertEqual(chromosome, 3)
        self.assertEqual(position, 52700001)
        for i in range(13):
            self.assertEqual(f[i], 0)
        self.assertAlmostEqual(f[13], .539275, delta=epsilon)
        self.assertAlmostEqual(f[161], .268882, delta=epsilon)


def main():
    unittest.main()


if __name__ == '__main__':
    main()

