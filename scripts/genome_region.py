#!/usr/bin/env python
#
# genome_region.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import unittest
import bisect


class IntegerSet:
    def __init__(self):
        self.intervals = []
    def insert(self, interval):
        index = bisect.bisect(self.intervals, interval)
        if index>0 and self.intervals[index-1][1] >= interval[0]: # overlap left
            previous_begin, previous_end = self.intervals[index-1]
            end = max(previous_end, interval[1])
            self.intervals[index-1] = (previous_begin, end) # replace interval
            index -= 1
        else:
            self.intervals.insert(index, interval) 
        self.merge_right(index)
        #self.validate() # for testing
    def merge_right(self, index):
        # print("merge_intervals:")
        # print("  before:", self.intervals)
        begin, end = self.intervals[index] 
        while index+1 < len(self.intervals):    # there is an interval to the right
            next_begin, next_end = self.intervals[index+1]
            if end < next_begin: break
            end = max(end, next_end)            # extend this interval
            del self.intervals[index+1]         # delete next
        self.intervals[index] = (begin,end)
        # print("  after:", self.intervals)
    def validate(self):
        for index, (begin,end) in enumerate(self.intervals):
            if index == 0: continue
            previous_begin, previous_end = self.intervals[index-1]
            if previous_end >= begin:
                print("Invalid:", self.intervals[index-1], self.intervals[index])
                print(self.intervals)
                raise Exception("Invalid intervals")
    def __len__(self):
        return sum([end-begin for (begin,end) in self.intervals])
    def __contains__(self, n):
        index = bisect.bisect(self.intervals, (n,float("inf")))
        if index == 0: return False
        begin, end = self.intervals[index-1]
        return n>=begin and n<end


class TestIntegerSet(unittest.TestCase):
    def test_insert_and_len(self):
        s = IntegerSet()
        s.insert((4,20))
        self.assertEqual(len(s), 16)
        s.insert((24,40))
        self.assertEqual(len(s), 32)
        s.insert((8,28))
        self.assertEqual(len(s.intervals), 1)
        self.assertEqual(len(s), 36)
        s.insert((50,60))
        s.insert((70,80))
        self.assertEqual(len(s.intervals), 3)
        self.assertEqual(len(s), 56)
        s.insert((0,65))
        self.assertEqual(len(s.intervals), 2)
        self.assertEqual(len(s), 75)
        s.insert((65,66))
        self.assertEqual(len(s.intervals), 2)
        self.assertEqual(len(s), 76)
        s.insert((-1,0))
        self.assertEqual(len(s.intervals), 2)
        self.assertEqual(len(s), 77)
        s.insert((66,70))
        self.assertEqual(len(s.intervals), 1)
        self.assertEqual(len(s), 81)
    def test_insert_and_len_2(self):
        s = IntegerSet()
        s.insert((4,20))
        self.assertEqual(len(s), 16)
        s.insert((24,40))
        self.assertEqual(len(s), 32)
        s.insert((22, 50))
        self.assertEqual(len(s), 44)
    def test_contains(self):
        s = IntegerSet()
        s.insert((4,20))
        s.insert((24,40))
        self.assertTrue(0 not in s)
        self.assertTrue(4 in s)
        self.assertTrue(20 not in s)
        self.assertTrue(23 not in s)
        self.assertTrue(24 in s)
        self.assertTrue(40 not in s)


class Locus:
    def __init__(self, chromosome_pair_index, position):
        self.chromosome_pair_index = chromosome_pair_index
        self.position = position


class GenomeRegion(IntegerSet):
    def __init__(self):
        IntegerSet.__init__(self)
        self.chromosome_lengths = [25000000, 50000000, 55000000] # X, 2L/2R, 3L/3R
        self.position_offsets = [0, 25000000, 75000000]
        self.genome_size = sum(self.chromosome_lengths)
    def insert_locus_region(self, chromosome_pair_index, position, radius):
        chromosome_length = self.chromosome_lengths[chromosome_pair_index]
        assert(position>=0 and position<chromosome_length)
        begin = max(0, position - radius) + self.position_offsets[chromosome_pair_index]
        end = min(chromosome_length, position + radius) + self.position_offsets[chromosome_pair_index]
        self.insert((begin,end))
    def __contains__(self, locus):
        return IntegerSet.__contains__(self,
            locus.position + self.position_offsets[locus.chromosome_pair_index])
    def insert_dgrp_unused(self):
        self.insert((0,148))                            # X begin
        self.insert((22422865,25000000))                # X end
        self.insert((25000000, 25000000+4851))          # 2L begin
        self.insert((25000000+23011491, 50000000))      # 2L end
        self.insert((50000000, 50000000+2782))          # 2R begin
        self.insert((50000000+21146771, 75000000))      # 2R end
        self.insert((75000000, 75000000+18636))         # 3L begin
        self.insert((75000000+24538562, 100000000))     # 3L end
        self.insert((100000000, 100000000+77))          # 3R begin
        self.insert((100000000+27899043, 130000000))    # 3R end


class TestGenomeRegion(unittest.TestCase):
    def test(self):
        region = GenomeRegion()
        region.insert_locus_region(0, 1000000, 10000)
        self.assertEqual(len(region), 20000)
        region.insert_locus_region(1, 1000000, 10000)
        self.assertEqual(len(region), 40000)
        region.insert_locus_region(2, 1000000, 10000)
        self.assertEqual(len(region), 60000)
        self.assertEqual(region.intervals, 
                         [(990000, 1010000), (25990000, 26010000), (75990000, 76010000)])
        region.insert_locus_region(0, 24999999, 9999)
        self.assertEqual(len(region), 70000)
        self.assertTrue((24990000,25000000) in region.intervals)
        region.insert_locus_region(1, 0, 10000)
        self.assertEqual(len(region), 80000)
        region.insert_locus_region(2, 20000, 100000)
        self.assertEqual(len(region), 200000)
        #print(region.intervals)
    def test_dgrp_unused(self):
        region = GenomeRegion()
        region.insert_dgrp_unused()
        # print(region.intervals)
        self.assertEqual(len(region), 11007762)
        region.insert((0, 22422865))            # X
        region.insert((25004851, 71146771))     # 2L/2R
        region.insert((75018636, 127899043))    # 3L/3R
        # print(region.intervals)
        self.assertEqual(region.genome_size, len(region))
    def test_contains(self):
        region = GenomeRegion()
        region.insert_locus_region(0, 1000000, 10000)
        region.insert_locus_region(1, 1000000, 10000)
        region.insert_locus_region(2, 1000000, 10000)
        self.assertTrue(Locus(0,1000000) in region)
        self.assertTrue(Locus(0,1100000) not in region)
        self.assertTrue(Locus(0,1009999) in region)
        self.assertTrue(Locus(0,1010000) not in region)
        self.assertTrue(Locus(0,990000) in region)
        self.assertTrue(Locus(0,989999) not in region)
        self.assertTrue(Locus(1,1000000) in region)
        self.assertTrue(Locus(2,1000000) in region)
        self.assertTrue(Locus(1,2000000) not in region)
        self.assertTrue(Locus(2,2000000) not in region)


