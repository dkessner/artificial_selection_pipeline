#!/usr/bin/env python
#
# calculate_average_power.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import os
import unittest
import numpy as np


class PowerCurve:
    def __init__(self, filename = ""):
        self.filename = filename
        if filename: 
            self.data = np.loadtxt(filename, skiprows=1)
            assert(len(self.data.shape)==2 and self.data.shape[1]==2)
    def power(self, fp_rate):
        index = np.searchsorted(self.data[:,0], fp_rate)
        if index == 0: return self.data[0,1]
        fp_rate_low, power_low = self.data[index-1]
        fp_rate_high, power_high = self.data[index] if index < self.data.shape[0] else (1.0,1.0)
        t = (fp_rate - fp_rate_low) / (fp_rate_high - fp_rate_low) if (fp_rate_high != fp_rate_low) else .5
        return power_low + t * (power_high - power_low)


class TestPowerCurve(unittest.TestCase):
    def test_interpolation(self):
        power_curve = PowerCurve()
        power_curve.data = np.zeros((11,2))
        for i in range(11):
            power_curve.data[i,0] = power_curve.data[i,1] = i/10.
        for i in range(101):
            fp_rate = i/100.
            self.assertEqual(fp_rate, power_curve.power(fp_rate))
        self.assertEqual(0, power_curve.power(0))
        self.assertEqual(0, power_curve.power(-1))
        self.assertEqual(1, power_curve.power(1))
        self.assertEqual(1, power_curve.power(2))
        self.assertEqual(1, power_curve.power(100))


def write_average_power_curve(filename, interval_length, interval_count, power_curves):
    if os.path.exists(filename):
        print("Output file", filename, "already exists.")
        sys.exit(1)
    with open(filename, 'w') as f:
        fp_rates = np.linspace(0, interval_length, interval_count + 1)
        print("fp_rate power", file=f)
        for fp_rate in fp_rates:
            average_power = np.mean([power_curve.power(fp_rate) for power_curve in power_curves])
            print(fp_rate, average_power, file=f)


def main():
    if len(sys.argv) < 4:
        print("Usage: calculate_average_power.py filename_output interval_count filename.power [...]")
        print("Calculates average power at interval_count evenly spaced points in [0, interval_length)")
        sys.exit(1)

    filename_output = sys.argv[1]
    interval_count = int(sys.argv[2])
    filenames = sys.argv[3:]

    if os.path.exists(filename_output):
        print("Output file", filename_output, "already exists.")
        sys.exit(1)

    power_curves = []

    for filename in filenames:
        try:
            power_curve = PowerCurve(filename)
            power_curves.append(power_curve)
        except Exception:
            print("[calculate_average_power.py] Warning: Bad input file", filename, file=sys.stderr)
            pass

    print("[calculate_average_power.py] Power curves:", len(power_curves), file=sys.stderr)

    write_average_power_curve(filename_output, 1, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom1", 1e-1, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom2", 1e-2, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom2_2", 2e-2, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom2_5", 5e-2, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom3", 1e-3, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom4", 1e-4, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom5", 1e-5, interval_count, power_curves)
    write_average_power_curve(filename_output + ".zoom6", 1e-6, interval_count, power_curves)


if __name__ == '__main__':
    main()

