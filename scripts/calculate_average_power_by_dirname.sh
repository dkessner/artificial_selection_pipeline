#!/bin/bash
#
# calculate_average_power_by_dirname.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ -f $HOME/.bash_profile ]; then source $HOME/.bash_profile; fi


results_dir=results


if [ -d $results_dir ]
then
    echo "Directory $results_dir already exists."
    exit 1
fi


mkdir $results_dir


for d in $(cat dirnames.txt); do
for method in single_site single_site_variance region_10kb region_100kb region_1mb; do
#for method in single_site single_site_variance region_10kb; do
    filename=$results_dir/$d.$method.average_power
    echo $filename
    calculate_average_power.py $filename 1000 $d/*.processed/*.$method.power
done; done


tar cjvf results.tbz $results_dir


