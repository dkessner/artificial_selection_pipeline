#!/bin/bash
#
# initialize_simulation.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


scriptdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


if [ $# -ne 2 ]
then
    echo "Usage: initialize_simulation.sh <simulation_name> <new_directory>"
    echo
    echo "Available simulations:"
    for simulation in $scriptdir/../simulations/*
    do
        simulation_basename=$(basename $simulation)
        if [ "$simulation_basename" != "common" ]
        then
            echo "  $simulation_basename"
        fi
    done
    exit 1
fi

simname=$1
dirname=$2

echo "scriptdir: $scriptdir"
echo "simname: $simname"
echo "dirname: $dirname"

if [ -e $dirname ]
then
    echo "Directory already exists: $dirname"
    exit 1
fi

cp -rL $scriptdir/../simulations/$simname $dirname
pushd $dirname > /dev/null
if [ -f init.sh ]; then ./init.sh; fi


