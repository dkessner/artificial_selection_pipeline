#!/bin/bash
#
# submit_job_array.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


taskid_low=1
taskid_high=$(ls *.simconfig | wc -l)

echo Ready to submit job array: $taskid_low $taskid_high
read

mkdir -p output
jobarray.q -jl $taskid_low -jh $taskid_high -ji 1 -e -t 2 -o output run_pipeline.sh $*

