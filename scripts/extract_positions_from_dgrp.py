#!/usr/bin/env python
#
# extract_positions_from_dgrp.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys



def main():
    if len(sys.argv) != 4:
        print("Usage: extract_positions_from_dgrp.py <snps_file> <position_begin> <position_end>\n")
        sys.exit(1)

    filename = sys.argv[1]
    position_begin = int(sys.argv[2])
    position_end = int(sys.argv[3])

    printing = False

    with open(filename) as f:
        print(f.readline(), end='')
        for line in f:
            position = int(line.split(',',1)[0])
            if not printing and position>=position_begin: printing = True
            if printing and position>=position_end: break
            if printing: print(line, end='')


if __name__ == '__main__':
    main()

