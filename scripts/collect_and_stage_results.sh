#!/bin/bash
#
# collect_and_stage_results.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


filename_dirnames=dirnames.txt

if [ ! -f $filename_dirnames ]
then
    echo $filename_dirnames not found.
    exit 1
fi

mkdir results
mkdir results/realized_s
mkdir results/focal_qtl_allele_frequencies

cp $filename_dirnames results
cp parameter_table.txt results

for dirname in $(cat $filename_dirnames)
do
    echo $dirname
    pushd $dirname > /dev/null
    make -f main.mak summary
    popd > /dev/null
    cp -r $dirname/realized_s results/realized_s/${dirname}_realized_s
    cp $dirname/focal_qtl_allele_frequencies.txt results/focal_qtl_allele_frequencies/${dirname}_focal_qtl_allele_frequencies.txt
    echo
done

tar cjvf results.tbz results

