#!/bin/bash
#
# run_song_pipeline.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#

run_count=$(ls *.simconfig | wc -l | tr -d ' ')

echo "Ready to run song_pipeline.mak: $run_count configurations <return>"
read

for f in *.simconfig
do
    make -f song_pipeline.mak ${f/simconfig/out}
done

