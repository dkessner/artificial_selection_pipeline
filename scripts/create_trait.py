#!/usr/bin/env python
#
# create_trait.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import numpy as np
import unittest
from math import *


class Config:

    def __init__(self, filename):
        self.parse_config_file(filename)
        self.parse_locus_list()
        self.parse_founder_haplotypes()
        self.report()
        self.validate()

    def parse_config_file(self, filename):
        # get parameters
        for line in open(filename):
            if line.startswith('#'): continue
            tokens = line.split()
            if not tokens: continue
            name, value = tokens
            if name == "seed": np.random.seed(int(value)) # optional
            elif name == "filename_output": self.filename_output = value
            elif name == "filename_output_summary": self.filename_output_summary = value
            elif name == "filename_founder_haplotypes": self.filename_founder_haplotypes = value
            elif name == "filename_locus_list": self.filename_locus_list = value
            elif name == "founder_population_size": self.founder_population_size = int(value)
            elif name == "total_variance": self.total_variance = float(value)
            elif name == "heritability": self.heritability = float(value)
            elif name == "focal_qtl_chromosome_pair_index": self.focal_qtl_chromosome_pair_index = int(value)
            elif name == "focal_qtl_position": self.focal_qtl_position = int(value)
            elif name == "focal_qtl_allele_frequency": self.focal_qtl_allele_frequency = float(value)
            elif name == "focal_qtl_effect_size": self.focal_qtl_effect_size = float(value)
        # compute derived fields
        self.genetic_variance = self.total_variance * self.heritability
        self.environmental_variance = self.total_variance - self.genetic_variance
        self.focal_qtl = (self.focal_qtl_chromosome_pair_index + 1, self.focal_qtl_position)
        self.has_focal_qtl = not (self.focal_qtl == (1,0) and self.focal_qtl_allele_frequency == 0.)

    def __str__(self):
        return "filename_output " + self.filename_output + "\n" + \
               "filename_output_summary " + self.filename_output_summary + "\n" + \
               "filename_founder_haplotypes " + self.filename_founder_haplotypes + "\n" + \
               "filename_locus_list " + self.filename_locus_list + "\n" + \
               "founder_population_size " + str(self.founder_population_size) + "\n" + \
               "total_variance " + str(self.total_variance) + "\n" + \
               "heritability " + str(self.heritability) + "\n" + \
               "focal_qtl_chromosome_pair_index " + str(self.focal_qtl_chromosome_pair_index) + "\n" + \
               "focal_qtl_position " + str(self.focal_qtl_position) + "\n" + \
               "focal_qtl_effect_size " + str(self.focal_qtl_effect_size) + "\n" + \
               "focal_qtl_allele_frequency " + str(self.focal_qtl_allele_frequency) + "\n"

    def validate(self):
        # required input parameters from config file
        for name in ["filename_output", "filename_output_summary", "filename_founder_haplotypes", 
            "filename_locus_list", "founder_population_size", "total_variance",
            "heritability", "focal_qtl_chromosome_pair_index", "focal_qtl_position",
            "focal_qtl_allele_frequency", "focal_qtl_effect_size"]:
            assert(name in self.__dict__)
        # derived fields
        for name in ["genetic_variance", "focal_qtl", "has_focal_qtl", "qtls", 
            "locus_list_name", "qtl_count", "focal_qtl_index", "snps"]:
            assert(name in self.__dict__)
        # sanity check: focal qtl variance
        def single_locus_genetic_variance(effect_size, allele_frequency):
            a = effect_size
            p = allele_frequency
            return (a**2)*2*p*(1-p)
        focal_qtl_variance = single_locus_genetic_variance(self.focal_qtl_effect_size, 
                                                           self.focal_qtl_allele_frequency) 
        assert(focal_qtl_variance < self.genetic_variance)

    def report(self):
        print("config:\n", self, sep='')
        print("founder haplotypes:", self.haplotype_count)
        print("qtls:", self.qtl_count)
        if self.has_focal_qtl:
            print("focal qtl:", self.focal_qtl)
        print("focal qtl index:", self.focal_qtl_index)
        print("target genetic variance:", self.genetic_variance)

    def parse_locus_list(self):
        self.qtls = []
        for line in open(self.filename_locus_list):
            tokens = line.split()
            if tokens[0] == "LocusList":
                self.locus_list_name = tokens[1]
            elif tokens[0] == 'chromosome:position':
                chromosome, position = int(tokens[2]), int(tokens[3])
                self.qtls.append((chromosome, position))
        self.qtl_count = len(self.qtls)
        self.focal_qtl_index = None
        if self.has_focal_qtl: self.focal_qtl_index = self.qtls.index(self.focal_qtl)

    def parse_founder_haplotypes(self):
        haplotypes = []
        for line in open(self.filename_founder_haplotypes):
            if line.startswith('#'): continue
            if line.startswith('segsites:'):
                segsites = int(line.split()[-1])
                assert(segsites == len(self.qtls))
            else: haplotypes.append(line.strip())
        assert(len(haplotypes) == 2 * self.founder_population_size)
        self.haplotype_count = len(haplotypes)
        # store snp values in 2-D array: (h,l) indexes haplotype, locus
        self.snps = np.zeros((self.haplotype_count, self.qtl_count), np.int)
        for h, haplotype in enumerate(haplotypes):
            for l, snp_value in enumerate(haplotype):
                self.snps[h,l] = int(snp_value)
        # sanity check: focal qtl allele frequency
        if self.has_focal_qtl:
            focal_qtl_allele_frequency = sum(self.snps[:,self.focal_qtl_index]) / float(self.haplotype_count)
            epsilon = 1e-6
            assert(abs(focal_qtl_allele_frequency - self.focal_qtl_allele_frequency) < epsilon)

    def scale_vector(self, scale):
        scale_vector = scale * np.ones(self.qtl_count)
        if self.has_focal_qtl: scale_vector[self.focal_qtl_index] = 1
        return scale_vector


def calculate_genetic_value(effect_sizes, haplotype1, haplotype2):
    return sum(effect_sizes*haplotype1 + effect_sizes*haplotype2)


def calculate_genetic_values(effect_sizes, snps):
    haplotype_count, locus_count = snps.shape
    population_size = haplotype_count/2
    genetic_values = np.zeros(population_size)
    for i in range(population_size):
        haplotype1 = snps[i*2, :]
        haplotype2 = snps[i*2 + 1, :]
        genetic_values[i] = calculate_genetic_value(effect_sizes, haplotype1, haplotype2)
    return genetic_values


class Test(unittest.TestCase):

    def test_calculate_genetic_value(self):
        effect_sizes = np.array([1,2,-3,4])
        h1 = np.array([0,1,0,1])
        h2 = np.array([1,1,1,1])
        self.assertEqual(calculate_genetic_value(effect_sizes, h1, h2), 10)

    def test_calculate_genetic_values(self):
        effect_sizes = np.array([1,2,3,4])
        snps = np.array([[0,1,0,1], [1,1,1,1],
                         [1,0,1,0], [0,0,0,0],
                         [1,1,1,1], [1,1,1,1]])
        genetic_values = calculate_genetic_values(effect_sizes, snps)
        self.assertTrue(np.array_equal(genetic_values, np.array([16, 4, 20])))


def generate_effect_sizes(config):
    effect_sizes = np.random.exponential(size=config.qtl_count)
    signs = np.random.choice(2, size=config.qtl_count) * 2 - 1
    effect_sizes *= signs
    if config.has_focal_qtl:
        effect_sizes[config.focal_qtl_index] = config.focal_qtl_effect_size
    epsilon = 1e-6
    for i in range(100):
        genetic_values = calculate_genetic_values(effect_sizes, config.snps)
        genetic_variance = np.var(genetic_values)
        # print("effect sizes:", effect_sizes)
        # print("genetic values:", genetic_values)
        # print("genetic variance:", genetic_variance)
        # print("iterations:", i)
        if abs(genetic_variance - config.genetic_variance) < epsilon: 
            print("effect sizes:", effect_sizes)
            print("genetic variance:", genetic_variance)
            print("iterations:", i)
            return effect_sizes
        scale = sqrt(config.genetic_variance / genetic_variance)
        scale_vector = config.scale_vector(scale)
        effect_sizes = effect_sizes * scale_vector
    raise Exception("[generate_effect_sizes] Max iterations exceeded.")


def write_trait(effect_sizes, config):
    # QuantitativeTrait_IndependentLoci trait
    #     environmental_variance = 0.005
    #     qtl = qtls[0] 0 100 1000
    #     qtl = qtls[1] 0 0.02 0.04
    with open(config.filename_output, 'w') as f:
        print("QuantitativeTrait_IndependentLoci trait", file=f)
        print("    environmental_variance =", config.environmental_variance, file=f)
        for i, (qtl,effect_size) in enumerate(zip(config.qtls, effect_sizes)):
            print("    qtl = ", config.locus_list_name, "[", i, "] 0 ", 
                  effect_size, " ", 2*effect_size, sep='', file=f)
    with open(config.filename_output_summary, 'w') as f:
        print("chromosome_pair_index position effect_size", file=f)
        for i, (qtl,effect_size) in enumerate(zip(config.qtls, effect_sizes)):
            chromosome, position = qtl
            chromosome_pair_index = chromosome - 1
            print(chromosome_pair_index, position, effect_size, file=f)


def main():

    if len(sys.argv) != 2:
        print("Usage: create_trait.py <config_file>")
        sys.exit(1)

    filename = sys.argv[1]
    config = Config(filename)
    effect_sizes = generate_effect_sizes(config)
    write_trait(effect_sizes, config)


if __name__ == '__main__':
    main()

