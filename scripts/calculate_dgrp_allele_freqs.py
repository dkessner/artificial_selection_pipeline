#!/usr/bin/env python
#
# calculate_dgrp_allele_freqs.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import os
import numpy as np
from haplotype_frequencies import HaplotypeFrequencies


def D(allele_freqs):
    return allele_freqs[2] - allele_freqs[0] + allele_freqs[3] - allele_freqs[1]


ACGT = ['A', 'C', 'G', 'T']


def process_chromosome_arm(chromosome_arm, dirname_dgrp, haplotype_frequencies):
    filename = "Variants_Sparse_" + chromosome_arm + ".sample_swap_fixed.txt"
    path_to_filename = os.path.join(dirname_dgrp, filename)
    with open(path_to_filename) as f:
        first_line = f.readline()
        assert(first_line.startswith(chromosome_arm + ',Ref,'))
        for line in f:
            tokens = line.split(',')
            assert(len(tokens) == 162 + 4)
            arm_position = int(tokens[0])
            ref = tokens[1]
            bases = tokens[2:-2]
            bits = np.array([int(base in ACGT and base!=ref) for base in bases])
            hapfreqs = []  
            valid_chromosome = 0
            valid_position = 0
            for population in range(1,5):
                chromosome, position, f = \
                    haplotype_frequencies.get_haplotype_frequencies(chromosome_arm, arm_position, population)
                if population == 1:
                    valid_chromosome = chromosome
                    valid_position = position
                else:
                    assert(valid_chromosome == chromosome and valid_position == position)
                hapfreqs.append(f)
            allele_freqs = [min(max(sum(f*bits),0),1) for f in hapfreqs]

            print(valid_chromosome, valid_position, end=' ')
            for allele_freq in allele_freqs:
                print(allele_freq, end=' ')
            print(D(allele_freqs))



def main():

    if len(sys.argv) != 3:
        print("Usage: calculate_dgrp_allele_freqs.py <dir_hapfreqs> <dir_dgrp_snps>")
        sys.exit(1)

    dirname_hapfreqs = sys.argv[1]
    dirname_dgrp = sys.argv[2]

    haplotype_frequencies = HaplotypeFrequencies(dirname_hapfreqs)

    print("chromosome position pop0 pop1 pop2 pop3 D") 
    chromosome_arms = ['X', '2L', '2R', '3L', '3R']
    for chromosome_arm in chromosome_arms:
        process_chromosome_arm(chromosome_arm, dirname_dgrp, haplotype_frequencies)

if __name__ == '__main__':
    main()

