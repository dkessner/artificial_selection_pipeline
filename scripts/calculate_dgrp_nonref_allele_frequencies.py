#!/usr/bin/env python
#
# calculate_dgrp_nonref_allele_frequencies.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import os
import numpy as np


def process_snps_file(filename):

    ACGT = set(['A', 'C', 'G', 'T'])
    chromosome_arms = ['X', '2L', '2R', '3L', '3R']
    haplotype_count = 162

    with open(filename) as f:
        tokens = f.readline().split(',')
        assert(len(tokens) == haplotype_count + 4 and tokens[0] in chromosome_arms)
        print("position ref nonref_allele_freq") 
        for line in f:
            tokens = line.split(',')
            assert(len(tokens) == haplotype_count + 4)
            position = int(tokens[0])
            ref = tokens[1]
            bases = tokens[2:-2]
            bits = np.array([int(base in ACGT and base!=ref) for base in bases])
            nonref_allele_freq = sum(bits)/162. 
            print(position, ref, nonref_allele_freq)


def main():

    if len(sys.argv) != 2:
        print("Usage: calculate_dgrp_nonref_freqs.py <dgrp_snps.txt>")
        print()
        print("Calculates non-ref allele frequency, forcing each position to be bi-allelic")
        print("(ref or ambiguous == 0; non-ref ACGT == 1)")
        print()
        print("Output: stdout")
        sys.exit(1)

    filename = sys.argv[1]
    process_snps_file(filename)


if __name__ == '__main__':
    main()

