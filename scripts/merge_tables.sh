#!/bin/bash
#
# merge_tables.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#

if [ $# -lt 1 ]
then
    echo "Usage: merge_tables.sh file1 [...]"
    echo
    echo "Merges tables with single header line omitted after the first file."
    exit 1
fi

cat $1
shift

for f in $*
do
    tail -n +2 $f
done

