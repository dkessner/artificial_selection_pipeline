#!/usr/bin/env python
#
# allele_frequencies_final_summary_to_D_QTL.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys



def main():
    if len(sys.argv) != 2:
        print("Usage: allele_frequencies_final_summary_to_D_QTL.py <filename>\n")
        sys.exit(1)

    filename = sys.argv[1]

    with open(filename) as f:
        first_line = f.readline()
        print("chromosome position D")
        for line in f:
            tokens = line.split()
            assert(len(tokens) == 4)
            D = abs(float(tokens[2]) - float(tokens[3]))
            print(tokens[0], tokens[1], D)
        

if __name__ == '__main__':
    main()

