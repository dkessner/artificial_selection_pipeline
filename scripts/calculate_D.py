#!/usr/bin/env python
#
# calculate_D.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import numpy as np



def D(allele_freqs):
    return allele_freqs[2] - allele_freqs[0] + allele_freqs[3] - allele_freqs[1]


def main():

    if len(sys.argv) != 3:
        print("Usage: calculate_D.py <filestem> <chromosome>\n")
        print()
        print("Input files allele_freqs_pop[i].txt:")
        print("  5000003 T 0.33836895")
        print()
        print("Output:")
        print("chromosome position pop1 pop2 pop3 pop4 D")
        sys.exit(1)

    filestem = sys.argv[1]
    chromosome = sys.argv[2]

    files = [open(filestem + str(pop) + ".txt") for pop in range(1,5)]

    print("chromosome position pop1 pop2 pop3 pop4 D")

    for line in files[0]:
        allele_freqs = np.zeros(4)
        position_0, ref_0, allele_freq_0 = line.split()
        allele_freqs[0] = float(allele_freq_0)
        for i,file in enumerate(files[1:]):
            pop = i + 1 # index change due to slicing
            position, ref, allele_freq = file.readline().split()
            assert(position == position_0 and ref == ref_0)
            allele_freqs[pop] = allele_freq
        print(chromosome, position, allele_freqs[0], allele_freqs[1], allele_freqs[2], allele_freqs[3],
                D(allele_freqs))
    

if __name__ == '__main__':
    main()

