#!/bin/bash
#
# run_pipeline.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


source $HOME/.bash_profile


if [ "$SGE_TASK_ID" == "" ]
then
    echo '$SGE_TASK_ID not set'
    exit 1
fi

id=$SGE_TASK_ID
targets=$id.out
make -f song_pipeline.mak $targets

