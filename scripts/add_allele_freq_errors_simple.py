#!/usr/bin/env python
#
# add_allele_freq_errors_simple.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import os
import random
from math import *
import numpy as np


class ErrorGenerator:
    def __init__(self, means, variances):
        assert(len(means) == len(variances))
        self.bin_count = len(means)
        self.means = means
        self.sds = [sqrt(variance) for variance in variances]
    def add_error(self, f):
        assert(f>=0 and f<=1)
        index = min(int(f * self.bin_count), self.bin_count-1)
        result = f + random.gauss(self.means[index], self.sds[index])
        if (result<0): result=0
        if (result>1): result=1
        return result


def parse_empirical_error_distribution(filename):
    for line in open(filename):
        tokens = line.split()
        if tokens[0] == "harp_diff_means": 
            harp_diff_means = np.array([float(token) for token in tokens[1:]])
        elif tokens[0] == "harp_diff_variances": 
            harp_diff_variances = np.array([float(token) for token in tokens[1:]])
        elif tokens[0] == "pileup_diff_means": 
            pileup_diff_means = np.array([float(token) for token in tokens[1:]])
        elif tokens[0] == "pileup_diff_variances": 
            pileup_diff_variances = np.array([float(token) for token in tokens[1:]])
        else:
            raise Exception("Error parsing empirical error distribution.")
    return ErrorGenerator(harp_diff_means, harp_diff_variances), \
           ErrorGenerator(pileup_diff_means, pileup_diff_variances)


def process_allele_freqs(filename_allele_freqs, error_generator):
    for line in open(filename_allele_freqs):
        tokens = line.split()
        assert(len(tokens) == 3)
        allele_freq = float(tokens[2])
        allele_freq_new = round(error_generator.add_error(allele_freq), 3) # .001 for N=1000
        print(tokens[0], tokens[1], allele_freq_new)


def main():

    if len(sys.argv) < 2:
        print("Usage: add_allele_freq_errors_simple.py <harp|pileup> file.allele_freqs [empirical_error_distribution.txt] [seed]\n")
        print()
        print(".allele_freqs input format:")
        print("    0 123456 0.012345")
        sys.exit(1)

    method = sys.argv[1]
    assert(method == "harp" or method == "pileup")
    filename_allele_freqs = sys.argv[2]

    script_dir = os.path.dirname(os.path.realpath(__file__))
    filename_error_dist = script_dir + "/empirical_error_distribution.txt"
    if len(sys.argv) > 3:
        filename_error_dist = sys.argv[3]
    if len(sys.argv) == 5:
        random.seed(int(sys.argv[4]))

    error_generator_harp, error_generator_pileup = \
        parse_empirical_error_distribution(filename_error_dist)

    process_allele_freqs(filename_allele_freqs, 
        error_generator_harp if method == "harp" else error_generator_pileup)


if __name__ == '__main__':
    main()

