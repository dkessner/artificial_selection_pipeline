#!/usr/bin/env python
#
# add_allele_freq_errors.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import os
import random
from math import *
import numpy as np


class ErrorGenerator:
    def __init__(self, means, variances):
        assert(len(means) == len(variances))
        self.bin_count = len(means)
        self.means = means
        self.sds = [sqrt(variance) for variance in variances]
    def add_error(self, f):
        assert(f>=0 and f<=1)
        index = min(int(f * self.bin_count), self.bin_count-1)
        result = f + random.gauss(self.means[index], self.sds[index])
        if (result<0): result=0
        if (result>1): result=1
        return result


def parse_empirical_error_distribution(filename):
    for line in open(filename):
        tokens = line.split()
        if tokens[0] == "harp_diff_means": 
            harp_diff_means = np.array([float(token) for token in tokens[1:]])
        elif tokens[0] == "harp_diff_variances": 
            harp_diff_variances = np.array([float(token) for token in tokens[1:]])
        elif tokens[0] == "pileup_diff_means": 
            pileup_diff_means = np.array([float(token) for token in tokens[1:]])
        elif tokens[0] == "pileup_diff_variances": 
            pileup_diff_variances = np.array([float(token) for token in tokens[1:]])
        else:
            raise Exception("Error parsing empirical error distribution.")
    return ErrorGenerator(harp_diff_means, harp_diff_variances), \
           ErrorGenerator(pileup_diff_means, pileup_diff_variances)


def D(allele_freqs):
    assert(len(allele_freqs) == 4)
    return allele_freqs[2] - allele_freqs[0] + allele_freqs[3] - allele_freqs[1]


def print_allele_freqs(allele_freqs):
    for allele_freq in allele_freqs: print(allele_freq, end=' ')
    print(D(allele_freqs), end=' ')


def process_allele_freqs(filename_allele_freqs, error_generator_harp, error_generator_pileup):
    with open(filename_allele_freqs) as f:
        line1 = f.readline().strip()
        if not line1.endswith("pop0 pop1 pop2 pop3 D"):
            raise Exception("Invalid allele frequency format.")
        print(line1, "pop0_harp pop1_harp pop2_harp pop3_harp D_harp pop0_pileup pop1_pileup pop2_pileup pop3_pileup D_pileup")
        for line in f:
            tokens = line.split()
            assert(len(tokens) >= 5)
            allele_freqs_true = [float(token) for token in tokens[-5:-1]]
            D = tokens[-1]
            allele_freqs_harp = [error_generator_harp.add_error(f) for f in allele_freqs_true]
            allele_freqs_pileup = [error_generator_pileup.add_error(f) for f in allele_freqs_true]
            print(' '.join(tokens[:-5]), end=' ')
            print_allele_freqs(allele_freqs_true)
            print_allele_freqs(allele_freqs_harp)
            print_allele_freqs(allele_freqs_pileup)
            print()


def main():

    if len(sys.argv) < 2:
        print("Usage: add_allele_freq_errors.py allele_frequencies_final_summary.txt [empirical_error_distribution.txt] [seed]\n")
        print()
        print("Allele freqs input format:")
        print("    [...] pop1 pop2 pop3 pop4 D")
        print("    [...] 0.33836895 0.31419956 0.07703922 0.61933495 0.04380566?2")
        print()
        print("Output:")
        print("[...] pop1 pop2 pop3 pop4 D pop0_harp pop1_harp pop2_harp pop3_harp D_harp pop0_pileup pop1_pileup pop2_pileup pop3_pileup D_pileup")
        sys.exit(1)

    filename_allele_freqs = sys.argv[1]

    script_dir = os.path.dirname(os.path.realpath(__file__))
    filename_error_dist = script_dir + "/empirical_error_distribution.txt"
    if len(sys.argv) >= 3:
        filename_error_dist = sys.argv[2]
    if len(sys.argv) == 4:
        random.seed(int(sys.argv[3]))

    error_generator_harp, error_generator_pileup = \
        parse_empirical_error_distribution(filename_error_dist)
    
    process_allele_freqs(filename_allele_freqs, error_generator_harp, error_generator_pileup)



if __name__ == '__main__':
    main()

