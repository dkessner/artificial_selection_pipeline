#!/usr/bin/env python
#
# merge_qtl_data.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys


def get_trait_summary_filename(filename_simconfig):
    for line in open(filename_simconfig):
        if line.startswith("#include trait"):
            trait_config_filename = line.split()[1]
            if not trait_config_filename.startswith("trait"):
                raise Exception("Something's wrong")
            return trait_config_filename.replace(".config.txt", ".summary.txt")


class QTLSummaryRecord:
    def __init__(self, line):
        tokens = line.split()
        if len(tokens) != 6:
            raise Exception("[QTLSummaryRecord] Bad format.")
        self.chromosome = int(tokens[0])
        self.position = int(tokens[1])
        self.effect_size = float(tokens[2])
        self.allele_frequency = float(tokens[3])
        self.qtl_variance = float(tokens[4])
        self.qtl_heritability = float(tokens[5])
    def __str__(self):
        # note: only print the fields we want to report (locus, effect size, initial allele Frequency)
        return str(self.chromosome) + ' ' + str(self.position) + ' ' + \
               str(self.effect_size) + ' ' + str(self.allele_frequency)
    def __repr__(self):
        return str(self)


class AlleleFrequencyRecord:
    def __init__(self, line):
        tokens = line.split()
        if len(tokens) != 7:
            raise Exception("[AlleleFrequencyRecord] Bad format.")
        self.chromosome = int(tokens[0])
        self.position = int(tokens[1])
        self.allele_frequencies = tokens[2:6]   # string
        self.D = tokens[6]                      # string
    def __str__(self):
        # note: only print allele freqs and D
        return ' '.join(self.allele_frequencies) + ' ' + self.D


def create_trait_summary_map(filename_trait_summary):
    trait_summary_map = {} # locus -> QTLSummaryRecord
    with open(filename_trait_summary) as f:
        line1 = f.readline().strip()
        if line1 != "chromosome position effect_size allele_frequency qtl_variance qtl_heritability":
            raise Exception("Bad trait summary format:\n" + line1)
        for line in f:
            record = QTLSummaryRecord(line)
            trait_summary_map[(record.chromosome, record.position)] = record
    return trait_summary_map


def process_allele_freqs(run_index, filename_allele_freqs, trait_summary_map):
    with open(filename_allele_freqs) as f:
        line1 = f.readline().strip()
        if line1 != "chromosome position pop0 pop1 pop2 pop3 D":
            raise Exception("Bad allele frequency summary format.")
        print("run chromosome position effect_size initial_allele_frequency pop0 pop1 pop2 pop3 D")
        for line in f:
            record = AlleleFrequencyRecord(line)
            if record.chromosome == 1 and record.position == 0: continue
            print(run_index, trait_summary_map[(record.chromosome, record.position)], record)


def main():

    if len(sys.argv) != 4:
        print("Usage: merge_qtl_data.py run_index filename.simconfig allele_frequencies_final_summary.txt\n")
        sys.exit(1)

    run_index = sys.argv[1]
    filename_simconfig = sys.argv[2]
    filename_allele_freqs = sys.argv[3]

    filename_trait_summary = get_trait_summary_filename(filename_simconfig)
    trait_summary_map = create_trait_summary_map(filename_trait_summary)

    process_allele_freqs(run_index, filename_allele_freqs, trait_summary_map)


if __name__ == "__main__":
    main()

