#!/bin/bash
#
# submit_job_arrays_run_10_sims_main_mak.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ $# -ne 1 ]
then
    echo "Usage: submit_job_arrays_run_10_sims_main_mak.sh <replicate_count>"
    exit 1
fi

replicate_count=$1
taskid_low=1
taskid_high=$(($replicate_count / 10))

job_array_count=$(wc -l dirnames.txt | awk '{print $1}')

echo Ready to submit $job_array_count job arrays with $taskid_high tasks each.
read

for dirname in $(cat dirnames.txt)
do
    echo $dirname
    pushd $dirname
    if [ -f trait_architecture.mak ]; then make -f trait_architecture.mak focal_qtl.dgrp.ms; fi
    mkdir -p output
    jobarray.q -jl $taskid_low -jh $taskid_high -ji 1 -e -t 2 -o output run_10_sims.sh
    popd
done


