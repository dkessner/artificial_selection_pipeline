#!/bin/bash

for arm in X 2L 2R 3L 3R
do
    echo $arm
    filename_in=Variants_Sparse_${arm}.sample_swap_fixed.txt
    filename_out=${arm}_nonref_allele_frequencies.txt
    if [ ! -f $filename_in ]
    then
        echo $filename_in not found.
        exit 1
    fi
    if [ ! -f $filename_out ]
    then
        echo Creating $filename_out
        calculate_dgrp_nonref_allele_frequencies.py $filename_in > $filename_out
    fi
done

