#!/usr/bin/env Rscript
#
# plot_power_vs_threshold.R
#
# Darren Kessner
# Novembre Lab, UCLA
#

suppressWarnings(library(ggplot2, quietly=T))

args=commandArgs(TRUE)

if (length(args) != 2)
{
    cat("Usage: plot_power_vs_threshold.R chromosome position\n")
    q(status=1)
}

chromosome = as.integer(args[1])
position = as.integer(args[2])

if (is.na(chromosome) || is.na(position))
{
    cat("Chromosome and position must be integers.", "\n")
    q(status=1)
}


t_allele_freqs = read.table("allele_freqs_summary_with_errors.txt", head=T)

t_summary = read.table("trait0.summary.txt", head=T) # TODO: cat trait summaries

t = merge(t_allele_freqs, t_summary, by=c("chromosome", "position"))

t_qtl = t[t$chromosome==chromosome & t$position==position,]

if (nrow(t_qtl) == 0)
{
    cat("Invalid chromosome or position:", chromosome, position, "\n")
    q(status=1)
}

e_qtl = ecdf(t_qtl$D)
e_qtl_harp = ecdf(t_qtl$D_harp)
e_qtl_pileup = ecdf(t_qtl$D_pileup)

#threshold = seq(.8,2,.01)
threshold = seq(2, .8, -.01)

#roc = data.frame(t=threshold, x=1-e_neutral(threshold), y=1-e_qtl(threshold))
#roc_harp = data.frame(t=threshold, x=1-e_neutral(threshold), y=1-e_qtl_harp(threshold))
#roc_pileup = data.frame(t=threshold, x=1-e_neutral(threshold), y=1-e_qtl_pileup(threshold))

# plot(roc$x, roc$y, type='l')
# lines(roc_harp$x, roc_harp$y, col='blue')
# lines(roc_pileup$x, roc_pileup$y, col='red')


filename = paste("power_vs_threshold.chr", chromosome, "_pos", position, ".pdf", sep='')
pdf(filename)

plot(threshold, 1-e_qtl(threshold), ylab="power")
lines(threshold, 1-e_qtl_harp(threshold), col='blue')
lines(threshold, 1-e_qtl_pileup(threshold), col='red')

dummy = dev.off()



