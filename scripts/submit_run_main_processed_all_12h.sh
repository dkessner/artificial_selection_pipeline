#!/bin/bash
#
# submit_run_main_processed_all_12h.sh
#
# Darren Kessner
# Novembre Lab, UCLA
#


if [ $# -ne 2 ]
then
    echo "Usage: submit_run_main_processed_all_12h.sh <taskid_low> <taskid_high>"
    exit 1
fi

taskid_low=$1
taskid_high=$2

job_array_count=$(wc -l dirnames.txt | awk '{print $1}')
echo Ready to submit $job_array_count job arrays, task ids $taskid_low to $taskid_high.
read

for d in $(cat dirnames.txt)
do
    echo $d
    pushd $d
    mkdir -p output
    jobarray.q -jl $taskid_low -jh $taskid_high -ji 1 -t 12 -d 2000 -o output run_main_processed.sh $*
    popd
done


