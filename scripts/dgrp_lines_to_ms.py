#!/usr/bin/env python
#
# dgrp_lines_to_ms.py
#
# Darren Kessner
# Novembre Lab, UCLA
#


from __future__ import print_function
import sys
import os
import numpy as np


chromosome_arms_ = ['X', '2L', '2R', '3L', '3R']


def parse_line_numbers(filename_line_numbers):
    line_numbers = {} # arm -> list of line numbers
    for line in open(filename_line_numbers):
        if line.startswith('#'): continue
        tokens = line.split()
        if len(tokens) == 0: continue
        arm, line_number = tokens
        assert(arm in chromosome_arms_)
        if arm not in line_numbers: line_numbers[arm] = []
        line_numbers[arm].append(int(line_number))
    return line_numbers


haplotype_count_ = 162


def extract_variants(line_numbers, dirname_dgrp):
    ACGT = ['A', 'C', 'G', 'T']
    variants = []
    for arm in chromosome_arms_:
        if arm not in line_numbers: continue
        line_numbers_to_extract = set(line_numbers[arm])
        filename = "Variants_Sparse_" + arm + ".sample_swap_fixed.txt"
        path_to_filename = os.path.join(dirname_dgrp, filename)
        with open(path_to_filename) as f:
            first_line = f.readline()
            assert(first_line.startswith(arm + ',Ref,'))
            line_number = 2
            for line in f:
                if line_number in line_numbers_to_extract:
                    line_numbers_to_extract.remove(line_number)
                    tokens = line.split(',')
                    assert(len(tokens) == haplotype_count_ + 4)
                    position = tokens[0]
                    locus = arm + "_" + position
                    ref = tokens[1]
                    bases = tokens[2:-2]
                    bitstring = ''.join([str(int(base in ACGT and base!=ref)) for base in bases])
                    variants.append((locus,bitstring))
                    if len(line_numbers_to_extract) == 0: break
                line_number += 1
    return variants


# X
# 2L: 0-23.1mb, 2R: 25-46.2mb
# 3L: 0-24.6mb, 3R: 25-52.9mb


def locus_string_to_forqs_locus(locus_string):
    tokens = locus_string.split('_')
    assert(len(tokens) == 2)
    arm = tokens[0]
    position = int(tokens[1])
    assert(arm in chromosome_arms_)
    if arm == 'X': 
        chromosome = 1
    elif arm == '2L':
        chromosome = 2
    elif arm == '2R':
        chromosome = 2
        position += 25000000
    elif arm == '3L':
        chromosome = 3
    elif arm == '3R':
        chromosome = 3
        position += 25000000
    else:
        raise Exception("This isn't happening")
    return chromosome, position


def main():

    if len(sys.argv) != 4:
        print("Usage: dgrp_lines_to_ms.py <filename_line_numbers> <dir_dgrp_snps> <output_stem>")
        print()
        print("filename_line_numbers:")
        print("# arm line_number")
        print("2L 578700")
        print("...")
        print("Output:")
        print("  <output_stem>.dgrp.ms")
        print("  <output_stem>.locus_list")
        sys.exit(1)

    filename_line_numbers = sys.argv[1]
    dirname_dgrp = sys.argv[2]
    output_stem = sys.argv[3]

    line_numbers = parse_line_numbers(filename_line_numbers)
    variants = extract_variants(line_numbers, dirname_dgrp)     

    if not line_numbers:
        print("[dgrp_lines_to_ms.py] No line numbers: creating empty output files.")
        with open(output_stem + ".dgrp.ms", 'w') as f: pass
        with open(output_stem + ".locus_list", 'w') as f: pass
        with open(output_stem + ".dgrp_allele_frequencies", 'w') as f: pass
        sys.exit(0)

    with open(output_stem + ".dgrp.ms", 'w') as f:
        print("#positions:", ' '.join([locus for (locus,bitstring) in variants]), file=f)
        print("segsites:", len(variants), file=f)
        for h in range(haplotype_count_):
            print(''.join([bitstring[h] for (locus,bitstring) in variants]), file=f)

    with open(output_stem + ".locus_list", 'w') as f:
        # LocusList qtls
        #     chromosome:position = 1 0
        #     chromosome:position = 2 10000000
        print("LocusList qtls", file=f)
        for (locus_string, bitstring) in variants:
            chromosome, position = locus_string_to_forqs_locus(locus_string)
            print("    chromosome:position =", chromosome, position, file=f)

    with open(output_stem + ".dgrp_allele_frequencies", 'w') as f:
        for (locus_string, bitstring) in variants:
            allele_frequency = bitstring.count('1')/float(len(bitstring))
            print(locus_string, allele_frequency, file=f)


if __name__ == '__main__':
    main()

